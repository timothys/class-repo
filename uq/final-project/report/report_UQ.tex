\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}

% --- Heading
\title{\vspace{-4ex}Stochastic Parameterization of Sub-grid Turbulence in a Low Resolution Ocean Model}
\author{Tim Smith}
\date{\vspace{-3ex}}

% --- Packages
\usepackage{geometry,amsmath,color,graphicx}
\geometry{margin=1in}
\begin{document}

%% --- New commands
\newcommand{\pderiv}[3][]{% \pderiv[<order>]{<func>}{<var>} 
  \ensuremath{\frac{\partial^{#1} {#2}}{\partial {#3}^{#1}}}}
\newcommand{\red}[1]{\textcolor{red}{#1}}

\maketitle

\section{Problem Statement}
  \label{problemStatement}
  
  Computational ocean models currently rely on deterministic parameterizations to represent turbulent processes that cannot be resolved in modern global grids. These parameters are determined so that the time-mean of buoyancy or momentum fluxes best fit observational data. Variability in these structures is often not well represented, however, since there is significant model error in representing a chaotic process with a set of time invariant variables. With lacking turbulent parameterizations, variability in sea level is not modeled well at the coarse grids necessary for global modeling.
  
  Here I discuss a Bayesian framework for estimating parameterizations of subgrid turbulence with a stochastic term in an idealized, low resolution ocean model. The stochastic component will serve to represent model inadequacy in the deterministic parameterizations, and my main consideration in this report is calibration and validation of the model. The necessary steps for this process are as follows:
  \begin{enumerate}
   \item Develop a modeling scenario which low resolution ocean models typically fail to accurately represent sea level variability. Here I focus on the wind generated gyre, and I discuss this in greater detail in Sections \ref{inputs} \& \ref{calibration}.
   \item Integrate a high resolution model that is forced with wind stress to produce a chaotic gyre. The data from this experiment will serve as observations of reality. This component will not include the stochastic model error term, and the key here is that the grid spacing must be fine enough to resolve turbulent processes. 
   \item Calibrate a low resolution model to the observations generated in the previous step, with the stochastic model inadequacy term included. 
   \item Use the coarse grid model output and observations to perform a posterior predictive check to compare, with uncertainty, the coarse grid output for sea level to the observations. 
   \item Assuming the observations and coarse grid output are not completely invalid, quantify the agreement between the two using the Highest Posterior Density metric.
  \end{enumerate}
  
  I will begin by outlining the mathematical framework for the experiments described in Section \ref{mathModel}. The reliable theory is outlined first, and I will then discuss the sources of uncertainty in the problem, including some of the other embedded models included amongst the reliable conservation laws. Boundary and initial conditions are discussed to set up a chaotic gyre in Section \ref{inputs}. Calibration of uncertain parameters for subgrid turbulence in an idealized set up is discussed in Section \ref{calibration}, and associated validation tests are covered in Section \ref{validation}. 
  
\section{Mathematical Framework}
  \label{mathModel}
  
  For this problem, the goal is to represent the evolution of sea surface height in an idealized ocean basin undergoing turbulent motion. Here it makes sense to use a simple model which represents the equations of motion well enough to be a qualitatively valid representation of observed ocean turbulence, without over complicating the situation beyond analysis. With this in mind, I represent the fluid flow with the shallow water equations based on a number of assumptions. In this section I start with reliable conservation laws and outline the necessary assumptions for implementing the shallow water equations as a representation for flow in an ocean basin.
  
  \subsection{Reliable Theory}
  \label{reliableTheory}
  
  The scales of problems in oceanographic modeling are well approximated with continuum mechanics. Quantum effects are negligible due to the large scale of the problem, and the speeds are such that relativistic effects are negligible as well. The basis for my problem is the conservation of mass and momentum, given generally by the equations: 
  
  \begin{equation}
   \int_{\Omega_t}\bigg(\frac{\partial \rho}{\partial t} + \nabla\cdot(\rho \mathbf{v})\bigg)d\mathbf{x} = 0
   \label{eq:continuityBasic}
  \end{equation}
  
  \begin{equation}
   \int_{\Omega_t}\bigg(\frac{D\rho\mathbf{v}}{Dt}\bigg)d\mathbf{x} = \int_{\Omega_t}\mathbf{f}(\mathbf{x},t)d\mathbf{x}
   \label{eq:momentumBasic}
  \end{equation}  
  
  where $\mathbf{f}(\mathbf{x},t)$ represents all of the forces present on the fluid. For the case of a non-rotating fluid, the forces acting on the fluid are due to pressure gradients, gravity, and friction (or viscosity) \cite{mandp}. I will term ``all other forces'' acting on the body as $\mathbf{f}_b$ for now. The force due to pressure gradients acting on the fluid is the representation of collective molecular motion acting on the external surface of a fluid partical. In differential form, this is $\mathbf{f}_p = \frac{\nabla p}{\rho}$, and I assume this form for the pressure force is part of my reliable theory (e.g. \cite{vallis}, \cite{jTin}). I also assume the force due to gravity has a highly reliable representation in differential form as $\mathbf{f}_g = \rho g \hat{\mathbf{k}}$, where I take $g$ to be a constant with no uncertainty. Viscous effects, representing molecular action on a fluid particle through a shear stress, are assumed to be reliably represented by $\mathbf{f}_v = \nu \nabla^2\mathbf{v}$ for Newtonian fluids like seawater \cite{vallis}. 
  
  For geophysical applications, rotational effects must also be taken into consideration. These effects add a Coriolis force: $\mathbf{f}_{Cor} = -2\mathbf{\Omega} \times \mathbf{v}$, and centrifugal force: $\mathbf{f}_{cent} = -\mathbf{\Omega} \times (\mathbf{\Omega} \times \mathbf{r})$, to the right hand side of Eqn. \ref{eq:momentumBasic}. Here $\mathbf{\Omega}$ is the rotation rate of the Earth and $\mathbf{r}$ is the radial distance from the center to a fluid particle. 
  
  \subsection{Assumptions}
  \label{assumptions}
  
    The shallow water equations are qualitatively a good option for modeling this system because they provide a tractable testbed for studying fluid motions similar to that seen in large scale patterns. Here I list the necessary assumptions that are made in using these equations. I still consider the assumptions made here to be part of the theory which needs not be questioned for my particular problem. However, it is worth noting that these are modeling \textit{assumptions} which are not applicable to all contexts of geophysical fluid dynamics. The list is as follows: 
    
    \begin{enumerate}
     \item The terms introduced from the centrifugal force are neglected since these terms are of the order $\frac{U^2}{R}$. Here $U$ is an approximate scale for horizontal velocity which we take to be $O(1) \, m/s$, which is typical for large scale flow patterns (\cite{vallis} or \cite{mandp}) and $R$ is the radius of the Earth, $R\sim O(10^9) \, m$.
     \item I assume that the vertical depth is much smaller than the lateral extent (i.e. the aspect ratio is small), as is the case for many oceanographic contexts. Considering the average depth of the ocean is $H\sim 3\times10^3 \, m$ and for ocean basin analyses the lateral extent is $L\sim 10^5 \, m$, the aspect ratio is $\sim O(10^{-2})$. As given in \cite{vallis}, this leads to hydrostatic balance: 
	 \begin{equation}
	   g = -\frac{1}{\rho}\pderiv{p}{z}
	   \label{eq:hydrostatic}
	 \end{equation}
      that is, the vertical pressure gradient is balance by gravitational effects. 
     \item Integrating Eqn. \ref{eq:hydrostatic} over the water column shows that the horizontal pressure gradient is solely represented by deviations in the free surface, $\eta$. That is:
	\begin{equation}
	 \pderiv{p}{x} = -\rho g \pderiv{\eta}{x}
	\end{equation}
	
	and similarly in the $y$ direction.      
     \item The equations of motion for the Earth are represented on a horizontal plane. This assumption follows from the fact that variations due to Earth's spherical shape are negligible over a the lateral distances being analyzed (other than the Coriolis effect, discussed next). 
     \item The horizontal plane is modified for the Coriolis term, such that it varies linearly with latitude. The Coriolis force is given by: 
	\begin{equation}
	  \mathbf{f}_{Cor} = -2\mathbf{\Omega}\times\mathbf{v} = 2\Omega \mathrm{sin}(\phi) \hat{\mathbf{k}}\times\mathbf{v} = f\hat{\mathbf{k}}\times\mathbf{v}
	\end{equation}
	where $f$ is the Coriolis parameter (an unfortunate choice given the variables used here, but it is used out of tradition). The approximation for the shallow water equations is made that 
	  \begin{equation}
	      f(y) \simeq 2f_0sin(\phi_0) + \beta y
	  \end{equation}
	where $f_0 \simeq 7.27 \mathrm{x} 10^{-5} s^{-1}$, and since $\beta = \pderiv{f}{y} \sim 10^{-11}$ $m^{-1}s^{-1}$ at mid latitudes, the variation is small enough to justify the assumption.

     \item The fluid is bounded below by a rigid surface and above by a free surface, $\eta$.
     \item Thermodynamic and chemical composition effects are ignored. Of course, expansion due to fluxes of heat or freshwater (e.g. from ice melt) play an important and complex role on sea level on a global scale. However, I make this assumption to specifically isolate deficiencies in turbulent momentum fluxes and their effects of sea level. Density is then taken to be a constant, $\rho_0$, since the dominant forces in effecting the density are temperature and salt content \cite{vallis}.
     \item Molecular viscosity effects are negligible for large scale geophysical applications. This arises from the fact that I assume time scales to be advective, since it is expected that this is the dominant process in turbulent motion. Taking, again $U\sim O(1) \, m/s$ and a length scale of $L\sim10^5 \, m$, advection can be scaled as:
	\begin{equation}
	 u\pderiv{u}{x} \sim \frac{U^2}{L} \sim O(10^{-5})
	\end{equation}
	 and molecular diffusion as: 
	 \begin{equation}
	  \nu \nabla^2u \sim \nu\frac{U}{L^2} \sim O(10^{-16})
	 \end{equation}
      since the molecular viscosity is $\nu \sim O(10^{-6})$ \cite{vallis}. We see that the advection term clearly dominates. Turbulent processes, however, depend on viscous effects for the dissipation of energy to small scales. This effect is parameterized with an embedded model, which is discussed in Section \ref{embeddedModels}.   
    \end{enumerate}
    
    With this set of assumptions, the shallow water equations are given by:
  
    \begin{equation}
    \pderiv{\eta}{t} + \pderiv{}{x}\Big(uh\Big) + \pderiv{}{y}\Big(vh\Big) = 0
    \label{eq:cont}
    \end{equation}
    \begin{equation}
    \pderiv{u}{t} + u\pderiv{u}{x} + v\pderiv{u}{y} - f v + g \pderiv{\eta}{x} = f_{b,u}
    \label{eq:umom}
    \end{equation}
    \begin{equation}
    \pderiv{v}{t} + u\pderiv{v}{x} + v\pderiv{v}{y} + f u + g \pderiv{\eta}{y} = f_{b,v}
    \label{eq:vmom}
    \end{equation}
    
    where the right hand side $\mathbf{f}_b = (f_{b,u},f_{b,v})$ includes additional embedded models and forcing terms at the boundary. These additional terms are discussed next. 
    
  \subsection{Further Assumptions: Embedded Models}
    \label{embeddedModels}
    
	In this section I discuss some of the less reliable assumptions and embedded models which are added to the right hand side of Eqns. \ref{eq:umom} \& \ref{eq:vmom}. 
	
    \subsubsection{Turbulent viscosity}
     \label{turbVisc}
	The first embedded model is a pseudo viscosity that is implemented in large scale ocean models. The use of this parameter stems from the study of wind driven gyres by Stommel and Munk, who introduced solutions to the Navier-Stokes equations in balanced flow \cite{vallis} \cite{munk}. The key here is that gyre circulations driven by zonal ($x$ direction) wind forcing develop a strong Western boundary current. Boundary currents of this nature are observed in the Atlantic as the Gulf stream and in the Pacific as the Kuroshio current. The momentum generated along the Western boundary in this situation must have some mechanism of dissipation, and in reality this is believed to be through an energy cascade from the large scale eddies to molecular dissipation. However, numerical models are lacking in their ability to reproduce this effect since small scale features are not resolved. Following the solution given by Munk, the form of this parameterized dissipative force is very similar to that of molecular viscosity:

	\begin{equation}
	  \mathbf{f}_m = \frac{1}{\rho_0 h}\kappa\nabla^2\mathbf{v}
	  \label{eq:eddyVisc}
	\end{equation}
	
	where $\mathbf{v}$ is the vector of horizontal velocity components, $h$ is the basin depth, and $\kappa$ is sometimes referred to as an Eddy viscosity. This is the less reliable parameter in this embedded model. As a modeling ansatz, the value for $\kappa$ is determined in order to resolve the Western boundary currents and provide numerical stability \cite{adcroft}, giving the restriction: 

	\begin{equation}
	  M_w = \pi\bigg(\frac{\kappa}{\beta}\bigg)^{1/3} > 3\triangle x
	  \label{eq:munkWidth}
	\end{equation}

	Here $M_w$ is the Munk width, which is essentially the boundary layer thickness associated with the Western current and $\triangle x$ is the grid spacing in a numerical model. Stommel's solution to a gyre formed from wind stress is shown in Figure \ref{fig:windStress}, which shows an illustratively similar (but less widely applicable \cite{vallis}) result to one which is obtained from Munk's solution. To my knowledge, there is no discussion of uncertainty regarding this parameter - it is merely noted as a constant in the model for documentation purposes (e.g. \cite{cooperAndZanna},\cite{ecco4}).
	
    \subsubsection{Eddy source term}
     \label{eddySource}

	The next embedded model I consider is for parameterizing upscale turbulent energy cascades. This embedded model has even less establishment in ocean modeling. The purpose here is to add the effect of chaotic motion as a model inadequacy term for unresolved on coarse grid ocean models, particularly in global configurations where computing to necessary resolutions is too costly. There are a number of methods for characterizing these processes in the literature (e.g. \cite{gm}). Recently, stochastic parameterization of this process has been studied in Quasi-Geostrophic models \cite{grooms} and shallow water systems \cite{cooperAndZanna}. These studies show success in representing the inverse energy cascade in subgrid turbulence with the following model: 

	\begin{equation}
	 \mathbf{f}_s = \frac{1}{\rho_0 h}\bigg(\mathbf{g}(x,y) + \mathbf{\epsilon}(x,y,t)\bigg)
	 \label{eq:turbForce}
	\end{equation}

    where $\mathbf{g}(x,y)$ is a spatially varying but constant in time ``turbulent forcing'', and $\mathbf{\epsilon}(x,y,t)$ is a stochastic process given by the stochastic differential equation:

	\begin{equation}
	  d\mathbf{\epsilon} = \mathbf{b}(x,y)dt + \mathbf{q}(x,y)d\mathbf{w} .
	  \label{eq:stoch}
	\end{equation}

	Here $\mathbf{w}$ is a Wiener process, and $\mathbf{b}$ and $\mathbf{q}$ control the time scale and amplitude of the stochastic process. Unfortunately, to my knowledge, no discussion of this model has included any formal description of uncertainty. It is simply shown to have better results in optimization studies for reproducing velocity fields undergoing wind stress which produces turbulent motion \cite{cooperAndZanna}.
        
  \subsection{Model Inputs}
    \label{inputs}
    \subsubsection{Boundary Conditions}
    \label{boundaryConditions}
  
      The final term included on the right hand side of the momentum equations is forcing due to wind stress:
      
	\begin{equation}
	 \mathbf{f}_{\tau} = \frac{\mathbf{\tau}}{\rho_0 h}
	\end{equation}

	which is applied at the ocean surface at each point on the grid. In ocean models with realistic geometric configurations (e.g. for ocean state estimation) wind stress is an input to the model through, e.g. 6-hour-averaged observations \cite{ecco4}. An example of this data is the ECMWF report which gives climatological mean and standard deviation estimates for wind stress at a resolution of $2.5^{\circ} \times 2.5^{\circ}$ across the globe \cite{trenberth}. In this study, I want to use the simplest model for analyzing the turbulent parameterizations discussed in Section \ref{embeddedModels}. In the ocean, wind stress is the main driver for turbulent processes (other than rough topography, which is not analyzed here), and this is particularly true in flows involving Western boundary currents. Typically for studies such as this one, wind stress is assumed to be purely zonal and is generated through a sinusoidal function varying with latitude such as: 
	
	  \begin{equation}
	   \tau^x \propto c_1\textrm{cos}(\lambda_1 y) + c_2\textrm{sin}(\lambda_2 y)
	   \label{eq:windStress}
	  \end{equation}
	  
	where $\tau^x$ is the zonal ($x$) component of wind stress and $\tau^y = 0$. This form of wind stress is qualitatively similar to the Westerlies which drive the gulf stream, and is useful in reproducing mesoscale ($\sim O(100km \times 100km)$) eddy formation in oceanic flow. A wind stress profile like this one is shown in Figure \ref{fig:windStress}, along with a solution for a balanced flow given by Stommel.
	  
	  \begin{figure}
	   \centering
	   \includegraphics[scale=0.5]{windstress}
	   \caption{Common choices for wind stress patterns when studying turbulence in wind driven gyre formations. The analytical solution for streamlines is also shown, when assuming the flow is steady and Stommel's representation for viscosity is used. The dense streamline contours on the Western boundary show there is rapid motion here, motivating the use of a viscosity for dissipating this increasing energy in a numerical model. The top figure represents qualitative similarities to the subtropical gyre the Atlantic, which is largely affected by Westerlies in the North and trade winds in the South. Wind stress similar to the bottom figure is useful for studying turbulence at the interface of two flows. Given the right choice of parameters in Eqn. \ref{eq:windStress}, a turbulent jet will form in the middle. This form of wind stress is studied in, e.g. \cite{cooperAndZanna}, \cite{berloff}, \cite{manaAndZanna} for parameterizing subgrid turbulence because it provides a wide range of scales in turbulent flow. Figure taken from \cite{vallis}.}
	   \label{fig:windStress}
	  \end{figure}
	  
    At the solid boundaries at the domain walls and bottom I would assume the no slip condition. This assumption follows from the development of wind-driven gyre circulation formulated by Stommel and Munk \cite{vallis}, and is used in a number of studies for turbulent parameterizations \cite{cooperAndZanna} \cite{manaAndZanna}.
    
    \subsubsection{Initial conditions}
    \label{initialConditions}
      
      The model is assumed to be initially at rest and is integrated forward in time to a point when initial transient processes have smoothed out. The initial conditions will then be taken to be the flow after this initial ``spin up'' period. For an ocean basin with a lateral extent of $\sim O(10^5) \, m$ is roughly $10^3$ days \cite{cooperAndZanna}. The reason for this is to represent turbulent processes which are exhibited in nature, without including artefacts in initializing a model. 
      

\section{Calibration}
  \label{calibration}
  
    In this section I discuss calibrating the set of parameters involved in the embedded model for upscale turbulent cascades (Section \ref{eddySource}). I do not include the parameter $\kappa$, for eddy viscosity for reasons discussed in Section \ref{likelihood}. Here the goal is to develop a calibration scenario which explores the ability of the embedded models to capture subgrid processes without complicating the set up to a point where uncertainties are no longer quantifiable or so large that they are useful. For this reason, I consider a constrained problem where ocean water governed by the shallow water equations in an idealized ``box'' geometry are forced with a wind stress profile governed by Eqn. \ref{eq:windStress}. I consider a Bayesian framework for calibration, which amounts to implementing Monte Carlo sampling methods for the posterior distribution: 
    
      \begin{equation}
       \pi(\mathbf{\theta} | \tilde{\eta}, X ) \propto \pi(\tilde{\eta} | \mathbf{\theta}, X) \pi(\theta | X)
       \label{eq:posterior}
      \end{equation}
      
      where $\mathbf{\theta} = (\mathbf{g}$, $\mathbf{b}$, $\mathbf{q})$ is my parameter set and $\tilde{\eta}$ represents sea level observations which the parameters are calibrated to. 
      
      \subsection{The Likelihood}
      \label{likelihood}
      
      The observations that I would use to calibrate the parameters $\mathbf{\theta}$ would need to be generated. For this problem, I would generate ``observations'' using the model outlined in Section \ref{mathModel} with a fine enough resolution to resolve processes which the embedded model given by Eqn. \ref{eq:turbForce} is to represent. Since I am generating this observation set with a numerical model, I will have to assume a value for $\kappa$ so that the solution is stable. In an ideal scenario, I would estimate this parameter with uncertainty as well, but here I would set it simply to satisfy the restriction in Eqn. \ref{eq:munkWidth}. 
	A configuration that could represent a ``truth model'' is discussed (albeit, without discussion of uncertainty) in \cite{cooperAndZanna}, and would be a good starting point for creating these observations. Assuming that we can find a resolution that is valid enough for this fine resolution requirement, I propose the following methodology for generating observations with uncertainty and forming a likelihood:

	\begin{enumerate}
	  \item Select an analytical form for wind stress given generally by Eqn. \ref{eq:windStress} in order to create chaotic gyre turbulence.
	  \item Next, I want to add a representation for observational uncertainty. Since in the real world wind stress is a common source of uncertainty, I will inject a random perturbation to the model for wind stress: 
	    \begin{equation}
	      \tau^x_{obs} = \tau^x + \xi 
	    \end{equation}
	  where $\xi \sim \mathcal{N}(0,\sigma^2_{\tau})$. I would represent the uncertainty in these random perturbations $\sigma_{\tau}$ based on representative values from estimates such as those given by Trenberth in \cite{trenberth}. 
	  \item I would then integrate the shallow water equations (\ref{eq:cont}-\ref{eq:vmom}) forward in time to get observations for $\eta$ over time, starting with initial conditions discussed in Section \ref{initialConditions}. Here I will integrate one model forward in time with wind stress represented deterministically as $\tau^x$ and another with the random component $\tau^x_{obs}$. The idea here is that the second model will represent observations while the first will represent what the model computes. I assume then that the difference between the two models is my uncertainty in observations, and I will assume this error is Gaussian. My reasoning for doing this is that I want to propagate error in wind stress observations, but have no method for doing so based on the shallow water equations. With these assumptions, I have the observation model given by:

	  \begin{equation}
	    \tilde{\eta} = H + \varepsilon_{obs}
	    \label{eq:likelihood}
	  \end{equation}

	  where $\varepsilon_{obs} \sim \mathcal{N}(0,\sigma^2)$ is the obserational uncertainty due to uncertainty in the wind stress field. The standard deviation in observations, $\sigma$ is unknown though, and this parameter has to be calibrated.
	  \item With the set of observations $\tilde{\eta}$ and model output $H$, I would calibrate $\sigma$ by forming the posterior through Monte Carlo sampling of the distributions: 
	    \begin{equation}
	      \pi(\sigma | \tilde{\eta}, X) \propto \pi(\tilde{\eta}|\sigma,X)\pi(\sigma|X)
	    \end{equation}
	  where 
	    \begin{equation}
	      \pi(\tilde{\eta}) \propto e^{-\frac{(\tilde{\eta}-H)^2}{2\sigma^2}}
	    \end{equation}
	  is the likelihood and I assume that the prior, $\pi(\sigma|X)$ is a Jeffrey's distribution since $\sigma$ represents a deviation from a nominal value and should be scale invariant. 
	\end{enumerate}
      
	This methodology will give me a set of observations $\tilde{\eta}$ with uncertainty $\sigma$ which I can use for forming the likelihood in Eqn. \ref{eq:posterior}. In this case the likelihood will be given by:

	  \begin{equation}
	    \pi(\tilde{\eta}) \propto e^{-\frac{(\tilde{\eta}-H_{coarse}(\mathbf{\theta}))^2}{2\sigma^2}}
	  \end{equation} 

	where $H_{coarse}(\mathbf{\theta})$ is the coarse model output for sea level height given the parameter values $\mathbf{\theta}$.
      
      \subsection{Prior distributions}
      \label{priors}
      
      There is little to no rigorous discussion of uncertainty regarding subgrid parameterizations in ocean modeling. These embedded models are unfortunately used with the implicit assertion of validity because they ``work''. Here I outline a best guess at prior distributions based on the little knowledge which is out there. I assume that each of the parameters are independent of one another, and the joint prior is given by the product: 
      
	\begin{equation}
	 \pi(\mathbf{\theta}|X) = \pi(\mathbf{g}|X)\pi(\mathbf{b}|X)\pi(\mathbf{q}|X) .
	 \label{eq:prior}
	\end{equation}

	I assume the following forms for each variable: 
	
	\begin{enumerate}
	 \item With little knowledge on the spatially varying but constant in time turbulent forcing, $\mathbf{g}$, I take its prior distribution to be uniform over a wide range. Here I would consider a range to include values which are shown in other subgrid forcing schemes, such as \cite{cooperAndZanna} or \cite{ecco4}. These studies merely give a range to work with, but not enough basis for any more knowledge to use a more informative prior. 
	 \item $\mathbf{b}$ can be thought of as a scaling parameter for the time scale of the stochastic process governed by Eqn. \ref{eq:stoch}. Therefore I would represent this with a Jeffrey's distribution where $\mathbf{b}$ is nonnegative. 
	 \item In a similar manner, $\mathbf{q}$ can be considered a scaling of the variance of the stochastic process in Eqn. \ref{eq:stoch}, and so I again represent it with a Jeffrey's distribution where $\mathbf{q}$ is nonnegative. 
	\end{enumerate}

\section{Validation}
\label{validation}
  
  The first validation test I would perform is a posterior predictive check. Here I would simply take samples from the posterior distribution of parameters (Eqn. \ref{eq:posterior}) in the forward model (the shallow water equations, Eqns. \ref{eq:cont} - \ref{eq:vmom}). I would integrate the coarse model over the same time span as the observations described in the calibration set up to see if the resulting estimates for sea level, $H_{coarse}$, based on these posterior distributions match observations, $\tilde{\eta}$, within uncertainty. Assuming the distributions are not terribly off, I would then compute the Highest Posterior Density to quantify the agreement between the two.
  
  As stated, this is the simplest of validation tests. However, the given definition of the embedded model for turbulent forcing (Eqn. \ref{eq:turbForce}) is too limited to extrapolate to other scenarios since the parameters $\mathbf{g}$, $\mathbf{b}$, \& $\mathbf{q}$ are functions of space. The reasoning here is that any change in the form of the wind stress will cause an entirely different form for each of these parameters, to match the resulting spatial structure of the turbulent flow. For example, the spatial structure of turbulent flow resulting from wind forcing like that in the top box of Figure \ref{fig:windStress} is much different than that resulting from the bottom. One could imagine, however, that the calibration experiment described in the previous section could allow us to derive a more informative form for this parameterization, such that these parameters are replaced by ones dependent on the flow characteristics rather than just space itself. With a better characterization of the subgrid turbulence, we could have a better basis for extrapolating to different scenarios. In this case, I could generate multiple ``observation sets'' as described in Section \ref{likelihood} using different wind stress patterns (e.g. those given in Figure \ref{fig:windStress}). I could then calibrate the parameter set to observations from one wind pattern, and then use these to see how the model predicts sea level given the other observations. This would be a much stronger validation test, but would only be necessary if the models passed the posterior predictive check described above. 
   
    
    
    
\newpage
\begin{thebibliography}{3}
  
  \bibitem{mandp}
  Marshall, J., \& Plumb, R. A. (2013). \textit{Atmosphere, ocean and climate dynamics: an introductory text.} (Vol. 21). Academic Press.
  
  \bibitem{vallis}
  Vallis, Geoffrey K. \textit{Atmospheric and oceanic fluid dynamics: fundamentals and large-scale circulation.} Cambridge University Press, 2006.
  
  \bibitem{jTin}
  Oden, J. Tinsley. \textit{An Introduction to Mathematical Modeling: A Course in Mechanics.} Vol. 1. John Wiley \& Sons, 2011.

  \bibitem{munk}
  Munk, Walter H. \textit{On the wind-driven ocean circulation.} Journal of meteorology 7.2 (1950): 80-93.

  \bibitem{adcroft}
  Adcroft, A. \textit{Numerical algorithms for use in a dynamical model of the ocean.} Diss. Ph. D. thesis, Imperial College, 1995.

  \bibitem{cooperAndZanna} 
  Cooper, Fenwick C., and Laure Zanna. \textit{Optimisation of an idealised ocean model, stochastic parameterisation of sub-grid eddies.} Ocean Modelling 88 (2015): 38-53.
  
  \bibitem{grooms}
  Grooms, Ian, Yoonsang Lee, and Andrew J. Majda. \textit{Numerical Schemes for Stochastic Backscatter in the Inverse Cascade of Quasigeostrophic Turbulence.} Multiscale Modeling \& Simulation 13.3 (2015): 1001-1021.
  
  \bibitem{berloff}
  Berloff, Pavel S. \textit{Random-forcing model of the mesoscale oceanic eddies.} Journal of Fluid Mechanics 529 (2005): 71-95.
  
  \bibitem{gm}
  Gent, Peter R., and James C. Mcwilliams. \textit{Isopycnal mixing in ocean circulation models.} Journal of Physical Oceanography 20.1 (1990): 150-155.
  
  \bibitem{ecco4} 
  Forget, Gael, et al. \textit{ECCO version 4: an integrated framework for non-linear inverse modeling and global ocean state estimation.} Geoscientific Model Development 8 (2015): 3071-3104.
  
  \bibitem{trenberth}
  Trenberth, K.E., W.G. Large, and J.G. Olson, 1989: \textit{A Global Ocean Wind Stress Climatology Based on ECMWF Analyses.} TN-338+STR, National Center for Atmospheric Research, 93 pp.
  
  \bibitem{manaAndZanna}
  PierGianLuca Porta Mana, Laure Zanna, \textit{Toward a stochastic parameterization of ocean mesoscale eddies}, Ocean Modelling, Volume 79, July 2014, Pages 1-20, ISSN 1463-5003, http://dx.doi.org/10.1016/j.ocemod.2014.04.002.

\end{thebibliography}

\end{document}

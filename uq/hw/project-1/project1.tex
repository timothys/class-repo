\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsfonts,amsthm,mathtools,graphicx,geometry}
\geometry{margin=1in}
\usepackage{enumitem} 
\usepackage{amsmath}
\title{\vspace{-4ex}UQ Project 1}
\author{Prakash Mohan\\
	     Clark Pederson  \\
	     Tim Smith \\
	     Maya Wei}
\date{\vspace{-3ex}}
%\date{}

\pdfinfo{%
  /Title    (UQ Project 1)
  /Author   ()
}
\graphicspath{ {/h2/tsmith/Documents/MATLAB/UQ/Projects/Project-1} }
\DeclareGraphicsExtensions{.pdf}

\begin{document}
  \maketitle
  
  \begin{enumerate}
   \item Here we seek prior probability distributions regarding the ``exact'' centerline velocity ($U_c$), the discretization error for the nominal mesh ($C$), and the convergence order ($p$). %
   For the centerline velocity we are given a ``best available value'' which we interpret to be the mean value of its underlying distribution: $\bar{U}_c \simeq 1.1627$. We are also given the notion of variance, since %
   95\% of the probability distribution is within $\pm 0.05\bar{U}_c$. %
   The \textit{a priori} standard deviation is then given by: %
    \begin{equation}
     \sigma_{U_c} = \dfrac{0.05\bar{U}_c}{z} ,
     \label{eq:sigu}
    \end{equation}
    \begin{equation}
     z = \dfrac{F^{-1}(0.975) - F^{-1}(0.025)}{2} ,
     \label{eq:z}
    \end{equation}
    where $F^{-1}(x)$ is the inverse cumulative distribution function. Here we assume that the probability function is symmetric and find $\sigma_{U_c}$ such that 95\% of the cumulative probability is centered about the mean. %
    With these data, we assume the prior probability distribution is Gaussian since this is the maximum entropy relative to a uniform ignorance. Thus, our prior distribution for $U_c$ is: %
    \begin{equation}
    \pi(U_c|X) \sim \mathcal{N}(\bar{U}_c,\sigma_{U_c}) . 
    \end{equation}
    We find the prior distribution for $C$ in a similar fashion. With no information leading us to believe $C$ is positive or negative, we estimate the prior mean to be: $\bar{C} = 0$. Since we know with 95\% confidence that %
    the error is less than $0.005\bar{U}_c$, we assume the standard deviation as we did in eqn. (\ref{eq:sigu}): %
    \begin{equation}
     \sigma_C = \dfrac{0.005\bar{U}_c}{z} ,
    \end{equation}
    where $z$ takes the same definition as in eqn. (\ref{eq:z}). With the same reasoning as before, our \textit{a priori} distribution for $C$ is assumed to be: %
    \begin{equation}
     \pi(C|X) \sim \mathcal{N}(\bar{C},\sigma_{C}) . 
    \end{equation}
    We have slightly less information on the convergence order. Here all we can assume is that the value for $p$ is between 1 and 10. With no more information we must assign equal probability to all possible values, and %
    so the distribution for $p$ is uniform: %
    \begin{equation}
     \pi(p|X) = 
     \begin{cases}
      1/9 , & 1 \leq p \leq 10 \\
      0 , & otherwise
     \end{cases} .
    \end{equation} 
    We also considered modeling the prior probability for $p$ with a Gamma distribution, since this has positive support and would have nonzero probability outside of our presumed range, $1\leq p \leq 10$. However, %
    we felt that the Gamma distribution gave unequal weighting to the values within this range that was not supported by our knowledge. For example, with shape $k=3$ and scale $\theta=2$ we found a much higher probability %
    around $p \sim 4$, so we opted for a uniform probability.
    
    With no further information about correlation between $U_c$, $C$, and $p$, we must assume \textit{a priori} that their distributions are independent from one another. Therefore, the joint prior probability distribution %
    is the product of the marginal distributions: %
    \begin{equation}
     \pi(U_c,C,p|X) = \pi(U_c|X) \pi(C|X) \pi(p|X)
     \label{eq:prior}
    \end{equation}
    where $X$ denotes all of the information above.
    
    \item We now must consider the likelihood. For $\tilde{U}_{ch} = {(y_1, y_2, ... y_n)}$ related to $(h_1, h_2, ... h_n)$ for $n$ data points. We assume that points $y_i$ are normally distributed around mean $U_{ch}$, %
    where $U_{ch_{i}} = U_{c} - Ch_i^p$, and the standard deviation $\sigma_i$ is given in the experiment table. The likelihood of four data points is equivalent to the four normal distributions multiplied together since %
    we assume these distributions are independent from one another.

    \begin{equation}\label{eq:likeli}
    \begin{split}
    \pi(\tilde{U}_{ch} | U_{c}, p, C, X) &= \frac{1}{(2\pi)^{n/2}} \Bigg(\prod_{i=1}^{n} \frac{1}{\sigma_i}\Bigg) \exp{{\frac{\sum y_i - U_{ch_{i}}}{2\sigma_i^2}}} \\
					 &= \frac{1}{(2\pi)^{n/2}} \Bigg(\prod_{i=1}^{n} \frac{1}{\sigma_i}\Bigg) \exp{\frac{\sum y_i - (U_c -Ch_i^p)}{2\sigma_i^2}} 
    \end{split}
    \end{equation}
    
   \item We implemented a Markov Chain Monte Carlo algorithm for estimating the posterior distributions for $U_c$, $C$, and $p$ based on the prior and likelihood given in eqns. (\ref{eq:prior}) \& (\ref{eq:likeli}). Figure %
   \ref{fig:subplot-given} shows our resulting posterior distributions with the given simulation parameters: 100 ``walkers'' with 5000 samples for the initial period and 10,000 samples for the posterior. %
    We then computed the posterior distributions with 250 ``walkers'' with twice as many samples in each period. The resulting distributions are shown in figures \ref{fig:subplot-us}-\ref{fig:p}. %
    The posterior distributions change very little with roughly twice the samples and walkers (compare figures \ref{fig:subplot-given} \& \ref{fig:subplot-us}), indicating that the simulation has produced a %
    reasonable representation for the posterior.
    \begin{figure}[h]
     \centering
     \includegraphics[width=\textwidth]{joint_post_original}
     \caption{Posterior distributions for $q=U_c$, $C$, and $p$ with 100 ``walkers'', 5,000 initial samples, and 10,000 samples for the posterior.}
   \label{fig:subplot-given}
   \end{figure}
   \begin{figure}[h]
     \centering
     \includegraphics[width=\textwidth]{joint_post}
     \caption{Posterior distributions for $q=U_c$, $C$, and $p$ with 250 ``walkers'', 10,000 initial samples, and 20,000 samples for the posterior.}
   \label{fig:subplot-us}
   \end{figure}
   \begin{figure}[h]
     \centering
     \includegraphics[width=.7\textwidth]{U_post}
     \caption{Prior and posterior distributions for $U=U_c$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:U}
   \end{figure}
   \begin{figure}[h]
     \centering
     \includegraphics[width=.7\textwidth]{C_post}
     \caption{Prior and posterior distributions for $C$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:C}
   \end{figure}
   \begin{figure}[h]
     \centering
     \includegraphics[width=.7\textwidth]{p_post}
     \caption{Prior and posterior distributions for $p$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:p}
   \end{figure}
  \item Our resulting posterior probability distributions are shown in figures \ref{fig:subplot-us}-\ref{fig:p}. The posterior distribution of the centerline velocity, $U_c$, %
  is much taller and narrower than the prior, which barely sits above the x-axis, indicating our increased certainty of plausible velocity values as shown in figure \ref{fig:U}. %
  Here we have a standard deviation of $\sigma_{U_c} = 3.5017e^{-4}$, which is substantially smaller than the prior value: $\sigma_{U_c} = 0.0297$. We also see that the resultant mean: $\bar{U}_c = 1.16359$, and %
  maximum a posteriori (MAP) estimate: $U_{c,MAP} = 1.16364$, are slightly greater %
  than the original value from the literature: $\bar{U}_c \simeq 1.1627$. We would expect this shift since our observed values are all higher than the prior mean. Also the near match between the mean and MAP estimate coincides %
  with the fact that the posterior distribution looks Gaussian. 
  
  The discretization error for the nominal mesh, $C$, is shown to be negative with high probability in figure \ref{fig:C}. The posterior distribution has a mean of $\bar{C} = -1.7106e^{-4}$ and MAP estimate of %
  $C_{MAP} = -5.4515e^{-5}$. %
  We find the negative shift in $C$ to be reasonable since the given data is larger than the prior mean value for $U_c$, and the relationship between the data and $C$: %
  \begin{equation}
   U_{ch} = U_c - C h^p 
  \end{equation}
  means that a negative value for $C$ increases values of $U_{ch}$. % 
  We again see that our certainty for the nominal mesh discretization error to be distributed as in \ref{fig:C} has increased since the standard deviation dropped by roughly an order of magnitude from the prior estimate: %
  $\sigma_C = 2.966e^{-3}$, to the posterior estimate: $\sigma_C = 3.3836e^{-4}$. 
  
  The convergence rate $p$, as shown in figure \ref{fig:p}, has a posterior mean of $\bar{p} = 5.513$, MAP estimate of $p_{MAP} =  4.9347$ and standard deviation $\sigma_p = 1.3529$. Thus, we would expect a convergence rate %
  close to $p \simeq 5$, which is in the center of convergence rates given \textit{a priori}.
  
  Figure \ref{fig:subplot-us} shows the posterior distributions of $C$ and $p$ are not independent since there is a notable curve in the joint contour plot. 
  \end{enumerate}
\end{document}
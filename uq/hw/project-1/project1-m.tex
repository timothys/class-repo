\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsfonts,amsthm,mathtools,graphicx,geometry}
\geometry{margin=1in}
\usepackage{enumitem} 
\usepackage{amsmath}
\title{\vspace{-4ex}UQ Project 1}
\author{Prakash Mohan\\
	     Clark Pederson  \\
	     Tim Smith \\
	     Maya Wei}
\date{\vspace{-3ex}}
%\date{}

\pdfinfo{%
  /Title    (UQ Project 1)
  /Author   ()
}
\graphicspath{ {/h2/tsmith/Documents/MATLAB/UQ/Projects/Project-1} }
\DeclareGraphicsExtensions{.pdf}

\begin{document}
  \maketitle
  
  \begin{enumerate}
   \item Here we seek prior probability distributions regarding the ``exact'' centerline velocity ($U_c$), the discretization error for the nominal mesh ($C$), and the convergence order ($p$). %
   For the centerline velocity we are given a ``best available value'' which we interpret to be the mean value of its underlying distribution: $\bar{U}_c \simeq 1.1627$. We are also given the notion of variance, since %
   95\% of the probability distribution is within $\pm 0.05\bar{U}_c$. %
   The \textit{a priori} standard deviation is then given by: %
    \begin{equation}
     \sigma_{U_c} = \dfrac{0.05\bar{U}_c}{z} ,
     \label{eq:sigu}
    \end{equation}
    \begin{equation}
     z = \dfrac{F^{-1}(0.975) - F^{-1}(0.025)}{2} ,
     \label{eq:z}
    \end{equation}
    where $F^{-1}(x)$ is the inverse cumulative distribution function. Here we assume that the probability function is symmetric and find $\sigma_{U_c}$ such that 95\% of the cumulative probability is centered about the mean. %
    With these data, we assume the prior probability distribution is Gaussian since this is the maximum entropy relative to a uniform ignorance. Thus, our prior distribution for $U_c$ is: %
    \begin{equation}
    \pi(U_c|X) \sim \mathcal{N}(\bar{U}_c,\sigma_{U_c}) . 
    \end{equation}
    We find the prior distribution for $C$ in a similar fashion. With no information leading us to believe $C$ is positive or negative, we estimate the prior mean to be: $\bar{C} = 0$. Since we know with 95\% confidence that %
    the error is less than $0.005\bar{U}_c$, we assume the standard deviation as we did in eqn. (\ref{eq:sigu}): %
    \begin{equation}
     \sigma_C = \dfrac{0.005\bar{U}_c}{z} ,
    \end{equation}
    where $z$ takes the same definition as in eqn. (\ref{eq:z}). With the same reasoning as before, our \textit{a priori} distribution for $C$ is assumed to be:
    \begin{equation}
     \pi(C|X) \sim \mathcal{N}(\bar{C},\sigma_{C}) . 
    \end{equation}
    We have slightly less information on the convergence order. Here all we can assume is that the value for $p$ is between 1 and 10. With no more information we must assign equal probability to all possible values, and %
    so the distribution for $p$ is uniform: %
    \begin{equation}
     \pi(p|X) = 
     \begin{cases}
      1/9 , & 1 \leq p \leq 10 \\
      0 , & otherwise
     \end{cases} .
    \end{equation} 
    We also considered modeling the prior probability for $p$ with a Gamma distribution, since this has positive support and would have nonzero probability outside of our presumed range, $1\leq p \leq 10$. However, %
    we felt that the Gamma distribution gave unequal weighting to the values within this range that was not supported by our knowledge. For example, with shape $k=3$ and scale $\theta=2$ we found a much higher probability %
    around $p \sim 4$, so we opted for a uniform probability.
    
    With no further information about correlation between $U_c$, $C$, and $p$, we must assume \textit{a priori} that their distributions are independent from one another. Therefore, the joint prior probability distribution %
    is the product of the marginal distributions: %
    \begin{equation}
     \pi(U_c,C,p|X) = \pi(U_c|X) \pi(C|X) \pi(p|X)
     \label{eq:prior}
    \end{equation}
    where $X$ denotes all of the information above.
    
    \item We now must consider the likelihood. For $\tilde{U}_{ch} = {(y_1, y_2, ... y_n)}$ related to $(h_1, h_2, ... h_n)$ for $n$ data points. We assume that points $y_i$ are normally distributed around mean $U_{ch}$, %
    where $U_{ch_{i}} = U_{c} - Ch_i^p$, and the standard deviation $\sigma_i$ is given in the experiment table. The likelihood of four data points is equivalent to the four normal distributions multiplied together since %
    we assume these distributions are independent from one another.

    \begin{equation}
    \begin{align}
    \pi(\tilde{U}_{ch} | U_{c}, p, C, X) &= \frac{1}{(2\pi)^{n/2}} \Bigg(\prod_{i=1}^{n} \frac{1}{\sigma_i}\Bigg) \exp{{\frac{\sum y_i - U_{ch_{i}}}{2\sigma_i^2}}} \\
					 &= \frac{1}{(2\pi)^{n/2}} \Bigg(\prod_{i=1}^{n} \frac{1}{\sigma_i}\Bigg) \exp{\frac{\sum y_i - (U_c -Ch_i^p)}{2\sigma_i^2}} 
    \label{eq:likeli}
    \end{align}
    \end{equation}
    
   \item We implemented a Markov Chain Monte Carlo algorithm for estimating the posterior distributions for $U_c$, $C$, and $p$ based on the prior and likelihood given in eqns. (\ref{eq:prior}) \& (\ref{eq:likeli}). Figure %
   \ref{fig:subplot-given} shows our resulting posterior distributions with the given simulation parameters: 100 ``walkers'' with 5000 samples for the initial period and 10,000 samples for the posterior. %
    We then computed the posterior distributions with 250 ``walkers'' with twice as many samples in each period. The resulting distributions are shown in figures \ref{fig:subplot-us}-\ref{fig:p}. %
    The posterior distributions change very little with roughly twice the samples and walkers (compare figures \ref{fig:subplot-given} \& \ref{fig:subplot-us}), indicating that the simulation has produced a %
    reasonable representation for the posterior.
    \begin{figure}[h]
     \centering
     \includegraphics[width=\textwidth]{joint_post_original}
     \caption{Posterior distributions for $U_c$, $C$, and $p$ with 100 ``walkers'', 5,000 initial samples, and 10,000 samples for the posterior.}
   \label{fig:subplot-given}
   \end{figure}
   \begin{figure}[ht]
     \centering
     \includegraphics[width=\textwidth]{joint_post}
     \caption{Posterior distributions for $U_c$, $C$, and $p$ with 250 ``walkers'', 10,000 initial samples, and 20,000 samples for the posterior.}
   \label{fig:subplot-us}
   \end{figure}
   \begin{figure}[ht]
     \centering
     \includegraphics[width=.7\textwidth]{U_post}
     \caption{Prior and posterior distributions for $U_c$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:U}
   \end{figure}
   \begin{figure}[ht]
     \centering
     \includegraphics[width=.7\textwidth]{C_post}
     \caption{Prior and posterior distributions for $C$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:C}
   \end{figure}
   \begin{figure}[ht]
     \centering
     \includegraphics[width=.7\textwidth]{p_post}
     \caption{Prior and posterior distributions for $p$, with sampling parameters as in figure \ref{fig:subplot-us}.}
   \label{fig:p}
   \end{figure}
  \end{enumerate}
\end{document}
\documentclass[a4paper,10pt]{scrartcl}
%\documentclass[a4paper,10pt]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{amsfonts,amsthm,mathtools,graphicx,geometry,tgtermes}
\geometry{margin=1in}
\usepackage{enumitem} 
\usepackage{amsmath}
\title{Homework 1}
\author{Tim Smith}
\date{\vspace{-5ex}}
%\date{}

\pdfinfo{%
  /Title    (UQ Exercise Set 1)
  /Author   (Tim Smith)
}
\graphicspath{ {/h2/tsmith/Documents/MATLAB/UQ/} }
\DeclareGraphicsExtensions{.pdf}

\begin{document}
  \maketitle

  \section*{Problem 1}
  \begin{enumerate}[label=(\alph*)]
   \item The math model is given by:%
   \begin{equation}
   \begin{split}
    ma &= F_g - F_b - F_d \\
       &= mg - \rho_fVg - F_d \\ 
       &= g(m-\rho_fVg) - F_d \\
   \end{split}
   \end{equation}
   where $m$ is the mass of the object, $a$ is its acceleration (directed downward, in the same direction as gravity), $F_g = mg$ is the force due to gravity, $F_b = \rho_fVg$ is the upward force due to buoyancy, and $F_d$ is a drag %
   force, due to resistance from the fluid. Here I make the following assumptions:
   \begin{enumerate}[label=(\roman*)]
    \item Newton's laws are sufficient in describing acceleration with respect to applied forces
    \item The force due to gravity is assumed to be a reliable theory or law for describing the force applied to an object in a gravitational field
    \item The motion is small enough that the position relative to the Earth's gravitational pull does not change (i.e. g = constant)
    \item Motion is large enough such that there are no quantum effects
    \item The velocity is small enough relative to the speed of light that there are no relativistic effects
    \item The upward force due to the ball's displacement of the the fluid is sufficiently described by the buoyancy relationship (as a law)
    \item The density $\rho_f$ and viscosity $\mu$ are assumed to be constant, homogeneous properties of the fluid
    \item The fluid is initially at rest 
    \item There is negligible heating due to friction 
    \item The motion can be described by the ball's velocity and acceleration
    \item The motion is one dimensional (neglecting any movements other than in the vertical)
    \item There are no vibrations in the room which could effect the motion (this partially counts in the previous assumption, because vibrations may perturb the motion in the horizontal)
    \item The ball is a perfect sphere with no imperfections
   \end{enumerate}
   The solution variable will be the velocity of the ball, $v$. The drag force, $F_d$ will require an embedded model and this should relate the properties of the fluid (e.g. $\mu$) to the ball's motion $v$.   
   \item The conditions required are:
    \begin{enumerate}[label=(\roman*)]
     \item Laminar flow (this requires a Newtonian fluid with a low Reynolds number)
     \item The ball is perfectly spherical
     \item The ball has uniform composition
     \item The surfaces of the ball are smooth
     \item There is no interaction between multiple balls (this is important for part c)
    \end{enumerate}
   \item Uncertainties arise from our inability to perfectly describe the phenomenon (model uncertainty) and from uncertainty in the parameters in our model. Here %
    at least some of the model uncertainties are:%
     \begin{enumerate}[label=(\roman*)]
	\item The usage of $\mu$ to characterize molecular interactions with macro-scale phenomena. This is a source of epistemic uncertainty since we could %
		ideally take measurements to obtain a value of $\mu$ which describes the motion to near certainty. Also %
		assuming that the fluid is isotropic introduces more uncertainty.
	\item There will be aleatoric uncertainty related to the motion of the ball, since there are likely to be random interactions between the %
		ball and the fluid which perturb its motion. 
	\item The fluid is assumed to be at rest except for where the ball passes through. There will likely be tiny motions in the fluid due to %
		random processes which we are unable to account for, introducing aleatoric uncertainty. 
	\item There is uncertainty in our embedded model (e.g. we neglect any higher order dependencies beyond velocity in our drag force term, we ignore advection) and the laws which we %
		 use to simplify reality into a meaningfully descriptive relationship. 
	\item There is uncertainty in how heat effects the system, which we neglect. Ideally we could model this as well, so I assume this is epistemic uncertainty.
     \end{enumerate}
  The uncertainties in each of the parameters are: %
     \begin{enumerate}[label=(\roman*)]
	\item Uncertainty in the value of $g$: there is slight uncertainty due to the motion in the experiment since this will technically change with height, and %
		there is some uncertainty (compared to the nominal value of $9.81 m/s^2$) due to our specific geographic location. We could measure this to %
		reduce our uncertainty here, so this is epistemic. 
	\item There is epistemic uncertainty in the mass and diameter of the ball, which we could measure to increase our certainty.
	\item There is uncertainty in the shape of the ball. We could theoretically measure and analyze any imperfections or deviations from a perfect sphere %
		so this is epistemic.
	\item There is uncertainty in the initial velocity that could be due to tiny motions before it's being dropped. This behavior is likely due to random %
		processes, or at least due to processes that are out of our control, and so it the uncertainty is aleatoric. 
	\item There is uncertainty in the density of the fluid. We could theoretically measure it to get a highly precise value so this is epistemic. 
     \end{enumerate}

   \item The complete statement of the mathematical model now involves the differential equation for the sphere's position with respect to time: %
      \begin{equation}
       \dfrac{d^2x}{dt^2} - \dfrac{6\pi\mu R}{m}\dfrac{dx}{dt} = \Big(1-\dfrac{\rho_fV}{m}\Big)g
      \end{equation}
      Solving for position with respect to time gives the relationship for height w.r.t. time:  %
      \begin{equation}
       x(t) = c_1\dfrac{m}{6\pi\mu R}e^{-\dfrac{6\pi\mu R}{m}t} - \dfrac{(1-\rho_fV)}{6\pi\mu R}gt + c_2 .
      \end{equation}
      Assuming the initial conditions: $x(t=0)=0$ and $\dot{x}(t=0)=0$ gives $c_1 = \dfrac{(\rho_fV-m)g}{6\pi\mu R}$ \& $c_2 = \dfrac{(\rho_fV-m)mg}{(6\pi\mu R)^2}$. This gives the model: %
      \begin{equation}
       x(t) = \dfrac{(\rho_fV-m)mg}{(6\pi\mu R)^2}\Big(e^{-\dfrac{6\pi\mu R}{m}t} + \dfrac{6\pi\mu R}{m}t + 1\Big)
      \end{equation}
      Now our mathematical model is to solve for the time, $t=T$, where $x=-L$ for our quantity of interest. This new process introduces the following uncertainties: %
	\begin{enumerate}[label=(\roman*)]
	 \item There is uncertainty in how the time is recorded, both when the ball is dropped and when it reaches a vertical distance $L$. This is epistemic because we could devise increasingly sophisticated %
		methods for increasing our certainty of a precise measurement (e.g. stopwatch -vs- laser system). 
	 \item The initial velocity of the spheres is subject to random perturbations (wind in the room, a shakey hand, etc.) which we can't perfectly account for. This introduces aleatoric uncertainty. 
	 \item There is uncertainty which depends on how the experiment is carried out. If the balls are repeatedly dropped from different locations (say lots of spheres are dropped into a pool of the fluid) %
		then there is additional uncertainty (on top of the previous point) on their initial height. If the balls are repeatedly dropped from the same location, we will be more certain about obtaining %
		a consistent initial position, so this is epistemic uncertainty. However, this would add uncertainty on the fluid characteristics, because we will have assumed that the fluid is homogeneous and at rest %
		this is epistemic though, because we could repeat the experiment and increase the waiting time between each drop to find how long it takes to reach ``steady state'' before repeating the drop. 
	\item There is aleatoric uncertainty on the properties of the spheres that we grab. For example, suppose 90\% of the balls are perfect spheres but 10\% are so imperfect that Stokes Law does not hold. % 
		Here we would expect to see 90\% of the spheres hit roughly the time estimated while 10\% should not. 
	\end{enumerate}
      The last scenario would be quite different if we selected one ball to repeat the experiment with, and the uncertainty introduced on our time estimate due to the selection process would be epistemic. In the example given, %
      we now have a 90\% chance of \textit{every} experiment ``succeeding'' and a 10\% chance of continually watching our model fail. 
  \item We could use this model to infer the value of viscosity that is most probable, after observing the sphere dropping a distance $L$ over time $T$. We would be interested in the posterior probability distribution: %
    \begin{equation}
     P(\mu|T,X) = \dfrac{P(T|\mu,X)P(\mu|X)}{P(T|X)}.
    \end{equation}
    The likelihood is a distribution for the predicted time for the ball to drop given the Newtonian viscosity $\mu$ and the background information $X$. %
    Here the background information, $X$, includes the mass and size of the ball, the distance that it is dropped ($L$), the gravitational constant, and the assumptions involved with applying Stokes Law. %
    The prior represents the probability of obtaining a Newtonian fluid with viscosity $\mu$, and our evidence normalizes the posterior. We could obtain the likelihood by computing the predicted time %
    for the ball to drop given our distribution of values for viscosity (these make up our prior). Then, given our measured time that we observe, we will have an idea of the probability that the viscosity %
    is described by the Newtonian model with value $\mu$. If our Newtonian assumption is way off, this probability should be low. We could reduce uncertainty in this process %
    by repeating the experiment many times (theoretically, we would want infinite experiments), and by reducing uncertainty in the calculation of time $T$ as described in the earlier parts. 
  
  \end{enumerate}

  
  \section*{Problem 2}
  \begin{enumerate}[label=(\alph*)]
   \item The probability that the die has one dot on $n=1$ faces is: %
    \begin{equation}
     P(1|X) = \Big(\dfrac{1}{6}\Big)\Big(\dfrac{5}{6}\Big)^5{6 \choose 1} ,
    \end{equation}
    and for $n=2$ faces, %
    \begin{equation}
     P(2|X) = \Big(\dfrac{1}{6}\Big)^2\Big(\dfrac{5}{6}\Big)^4{6 \choose 2} .
    \end{equation}
    In general (including $n=0$), this becomes: %
    \begin{equation}
     P(n|X) = \Big(\dfrac{1}{6}\Big)^n\Big(\dfrac{5}{6}\Big)^{(6-n)}{6 \choose n} .
    \end{equation}
    Here, the background information, $X$, includes the following:
    \begin{enumerate}[label=(\roman*)]
     \item The die has six sides 
     \item Each side has a number of dots ranging 1-6 on it
     \item The number of dots on each side is not necessarily unique, so %
	    some numbers may not be represented
     \item More specifically, the number of dots printed on each side is randomly %
	    selected, where each face is printed independently with an equal %
	    probability of 1-6 dots 
    \end{enumerate}
   \item Given that the die is thrown $N$ times and we know that a one has shown up %
	 $m\leq N$ times, the updated probability that there are $n$ faces with one dot is: %
	 \begin{equation}
	  P(n|m,N,X) = \dfrac{P(m|n,N,X)P(n|X)}{P(m|X)} .
	 \end{equation}
	 Here the prior is $P(n|X)$, which is given in the previous part as the probability %
	 of one dot being printed on $n$ faces \textit{before} the die was rolled. %
	 The likelihood here is the probability of seeing one dot $m$ times, given %
	 $N$ die rolls with a perfectly weighted die that has $n$ faces with one dot on it %
	 (other background information given by $X$ as stated previously. I computed the %
	 general form as follows:
	 \begin{equation}
	  P(m=1|n=1,N=2,X) = \Big(\dfrac{1}{6}\Big)\Big(\dfrac{5}{6}\Big){2 \choose 1}
	 \end{equation}
	 \begin{equation}
	  P(m=1|n=2,N=3,X) = \Big(\dfrac{2}{6}\Big)\Big(\dfrac{4}{6}\Big)^2{3 \choose 1}
	 \end{equation}
	 \begin{equation}
	  P(m=2|n=2,N=3,X) = \Big(\dfrac{2}{6}\Big)^2\Big(\dfrac{4}{6}\Big){3 \choose 2}
	 \end{equation}
	 So, in general: %
	 \begin{equation}
	  P(m|n,N,X) = 
	  \begin{cases}
	   \Big(\dfrac{n}{6}\Big)^m\Big(\dfrac{6-n}{6}\Big)^{N-m}{N \choose m}, &  0<n<6 \\
	   1, & m=0, n=0 \\
	   0, & 0<m\leq N, n=0 \\
	   0, & m=0, n=6 \\
	   1, & 0<m\leq N, n=6
	  \end{cases}
	 \end{equation}
	 The evidence here is the probability of seeing one dot show up $m$ times, given %
	 $N$ die rolls. This is simply the sum of products of the prior and likelihood:
	 \begin{equation}
	  P(m|N,X) = \sum_{n=0}^{n=6}P(m|n,N,X)P(n|X) .
	 \end{equation}
   \item The simulated posterior probability distribution is shown for each $n$ in figure \ref{fig:bigplot}. The probability increases exponentially, and after roughly 40 tosses of the die we can be sure of $n$ being 0 or 6. For %
   $n$ somewhere in between, it takes a little longer for the noise to smooth out. We can be \textit{almost} certain of the value of $n$ (i.e. the probability is 99\%) for all $n$ after $N \simeq 300$.  
   \begin{figure}[h]
    \centering
    \includegraphics[trim= 0cm 4cm 0cm 3cm, clip=true, width=.75\textwidth]{hw1-2}
    \caption{$P(n|m,N,X)$ is shown for each value of $n$ as a function of $N$}
    \label{fig:bigplot}
   \end{figure}
   \section*{Problem 3}
   \begin{enumerate}[label=(\alph*)]
    \item The background information, $X$ is: %
     \begin{enumerate}[label=(\roman*)]
      \item The data, $D = {(x_i,y_i), i=1, ..., N}$, are linearly related as %
      \begin{equation}\label{eq:lin}
       y_i = ax_i+b
      \end{equation}
      \item The measured data $x_i$ are assumed to be exact, with no uncertainty
      \item The measurements $y_i$ are independent from one another
      \item The measured data $y_i$ are normally distributed with standard deviation $\sigma_i$, so: %
      \begin{equation}
       \tilde{y}_i = y_i + \varepsilon_i
      \end{equation}
      where $\tilde{y}_i$ is the measured value, $y_i$ is the value given by the model in eqn. \ref{eq:lin}, %
      and $P(\varepsilon_i|X)=\mathcal{N}(0,\sigma_i^2)$. Here I am assuming that the linear model %
      in eqn. \ref{eq:lin} is ``good enough'' to represent the mean value for $y_i$, about which %
      we see deviations, $\sigma_i$, to give the measured $\tilde{y}_i$.
      \item The values $(a,b)$ in eqn. \ref{eq:lin} are assumed to be distributed as: %
      $P(a|X)=\mathcal{N}(a_0,\sigma_a) $ and $P(b|X)=\mathcal{N}(b_0,\sigma_b) $
      \item The distributions $P(a|X)$ and $P(b|X)$ are independent from one another
     \end{enumerate}
    \item Since $a$ and $b$ are independently distributed, the joint prior is the product %
    of each individual distribution:
    \begin{equation}
     P(a,b|X) = P(a|X)P(b|X) = \Bigg(\dfrac{1}{\sigma_a\sqrt{2\pi}}e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}}\Bigg) %
     \Bigg(\dfrac{1}{\sigma_b\sqrt{2\pi}}e^{-\dfrac{(b-b_0)^2}{2\sigma_b^2}}\Bigg)
    \end{equation}
    \begin{equation}
     P(a,b|X) = \dfrac{1}{\sigma_a\sigma_b2\pi}e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}}
    \end{equation}
    \item The likelihood of observing the data, $D$, given that the parameters in eqn. \ref{eq:lin} %
    have taken on the values $(a,b)$ is:
    \begin{equation}
    \begin{split}
     P(D|a,b,X) &= \prod_{i=1}^N\dfrac{1}{\sigma_i\sqrt{2\pi}}e^{-\dfrac{(\tilde{y}_i - y_i)^2}{2\sigma_i^2}} \\
		&= \prod_{i=1}^N\dfrac{1}{\sigma_i\sqrt{2\pi}}e^{-\dfrac{(\tilde{y}_i - a\tilde{x}_i - b)^2}{2\sigma_i^2}} ,
    \end{split}
    \end{equation}
    where I use $(\tilde{x}_i,\tilde{y}_i)$ to denote measured values, and $y_i$ to denote values obtained from %
    the model. Since the measurements in $x_i$ are assumed to be entirely certain, $\tilde{x}_i = x_i$. %
    Note that I take the product over all measurements since the data are assumed to be independently distributed.
    \item The joint posterior distribution is the updated probability of the parameters in eqn. \ref{eq:lin} %
    taking on the values $(a,b)$, given that we have observed the measured data $D = {(\tilde{x}_i,\tilde{y}_i), i=1,...,N}$. %
    This probability is given by the expression:
    \begin{equation}\label{eq:post}
    \begin{split}
        P(a,b|D,X) &= \dfrac{P(D|a,b,X)P(a,b|X)}{P(D|X)} \\
		   &= \dfrac{\prod_{i=1}^N\dfrac{1}{\sigma_i\sqrt{2\pi}}e^{-\dfrac{(\tilde{y}_i - a\tilde{x}_i - b)^2}{2\sigma_i^2}}%
			  \dfrac{1}{\sigma_a\sigma_b2\pi}e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}}}%
			 {\int_{-\infty}^{\infty}\int_{-\infty}^{\infty}P(D|a,b,X)P(a,b|X)\,da\,db} \\
		   &= \dfrac{\dfrac{1}{(2\pi)^{N/2+1}\sigma_a\sigma_b}\prod_{i=1}^N\Big(\dfrac{1}{\sigma_i}\Big)e^{%
			   -\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}-\sum_{i=1}^N%
			    \dfrac{(\tilde{y}_i-a\tilde{x}_i-b)^2}{2\sigma_i^2}}}%
			 {\int_{-\infty}^{\infty}\int_{-\infty}^{\infty}P(D|a,b,X)P(a,b|X)\,da\,db} .
    \end{split}
    \end{equation}
    \item The values for $(a,b)$ at the maximum of the posterior distribution are given by setting the partials %
    $\dfrac{\partial P(a,b|D,X)}{\partial a}, \dfrac{\partial P(a,b|D,X)}{\partial a}$ equal to zero. Since only the exponential %
    function is dependent on the parameter values, we maximize this function as follows:
    \begin{equation}
     \begin{split}
        &0 = \dfrac{\partial}{\partial a}\Bigg(e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}-\sum_{i=1}^N%
			    \dfrac{(\tilde{y}_i-a\tilde{x}_i-b)^2}{2\sigma_i^2}}\Bigg) \\
	 &= -\Bigg(\sum_{i=1}^N\dfrac{(\tilde{y}_i-a\tilde{x}_i-b)\tilde{x}_i}{\sigma_i^2}+\dfrac{a-a_0}{\sigma_a^2}\Bigg)%
	      e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}-\sum_{i=1}^N%
			    \dfrac{(\tilde{y}_i-a\tilde{x}_i-b)^2}{2\sigma_i^2}}
     \end{split}
    \end{equation}
    \begin{equation}
     \begin{split}
        &0 = \dfrac{\partial}{\partial b}\Bigg(e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}-\sum_{i=1}^N%
			    \dfrac{(\tilde{y}_i-a\tilde{x}_i-b)^2}{2\sigma_i^2}}\Bigg) \\
	 &= -\Bigg(\sum_{i=1}^N\dfrac{\tilde{y}_i-a\tilde{x}_i-b}{\sigma_i^2}+\dfrac{b-b_0}{\sigma_b^2}\Bigg)%
	      e^{-\dfrac{(a-a_0)^2}{2\sigma_a^2}-\dfrac{(b-b_0)^2}{2\sigma_b^2}-\sum_{i=1}^N%
			    \dfrac{(\tilde{y}_i-a\tilde{x}_i-b)^2}{2\sigma_i^2}}.
     \end{split}
    \end{equation}
    We can only have a maximum when $(a,b)$ satisfy:
    \begin{equation}
     \sum_{i=1}^N\dfrac{(\tilde{y}_i-a\tilde{x}_i-b)\tilde{x}_i}{\sigma_i^2}+\dfrac{a-a_0}{\sigma_a^2} = 0
    \end{equation}
    \begin{equation}
      \sum_{i=1}^N\dfrac{\tilde{y}_i-a\tilde{x}_i-b}{\sigma_i^2}+\dfrac{b-b_0}{\sigma_b^2} = 0 ,
    \end{equation}
    and so the parameters $a$ and $b$ at the mode are given by the solution to the system of equations:
    \begin{equation}
     \begin{pmatrix}
      \sum_{i=1}^N\dfrac{\tilde{x}_i^2}{\sigma_i^2}-\dfrac{1}{\sigma_a^2} & \sum_{i=1}^N\dfrac{\tilde{x}_i}{\sigma_i^2} \\
      \sum_{i=1}^N\dfrac{\tilde{x}_i}{\sigma_i^2}   & \sum_{i=1}^N\dfrac{1}{\sigma_i^2} - \dfrac{1}{\sigma_b^2}
     \end{pmatrix}
     \begin{pmatrix}
      a \\ b
     \end{pmatrix}
     +
     \begin{pmatrix}
      \dfrac{a_0}{\sigma_a^2} \\ \dfrac{b_0}{\sigma_b^2}
     \end{pmatrix}
      = 
      \begin{pmatrix}
       \sum_{i=1}^N\dfrac{\tilde{x}_i\tilde{y}_i}{\sigma_i^2} \\
       \sum_{i=1}^N\dfrac{\tilde{y}_i}{\sigma_i^2}
      \end{pmatrix}.
    \end{equation}
    \item Since the measurements are independently distributed, the variance-covariance matrix becomes the diagonal matrix with entries %
    $R_{ii} = \sigma_i$ so that the weights are given by $W_{ii} = R_{ii}^{-1} = \dfrac{1}{\sigma_i^2}$. Thus, the solution to the %
    weighted least squares problem is given by:
    \begin{equation}
     J = \dfrac{1}{2}\sum_{i=1}^N\Big(\tilde{y}_i - a\tilde{x}_i - b\Big)^2W_{ii}
    \end{equation}
    \begin{equation}
     \dfrac{\partial J}{\partial a} = \sum_{i=1}^N\dfrac{(\tilde{y}_i - a\tilde{x}_i - b)(-\tilde{x}_i)}{\sigma_i^2} = 0
    \end{equation}
    \begin{equation}
     \dfrac{\partial J}{\partial b} = \sum_{i=1}^N\dfrac{(\tilde{y}_i - a\tilde{x}_i - b)(-1)}{\sigma_i^2} = 0 ,
    \end{equation}
    which gives the system of equations%
    \begin{equation}
     \begin{pmatrix}
      \sum_{i=1}^NN\dfrac{\tilde{x}_i^2}{\sigma_i^2} & \sum_{i=1}^N\dfrac{\tilde{x}_i}{\sigma_i^2} \\
      \sum_{i=1}^N\dfrac{\tilde{x}_i}{\sigma_i^2}   & \sum_{i=1}^N\dfrac{1}{\sigma_i^2}
     \end{pmatrix}
     \begin{pmatrix}
      a \\ b
     \end{pmatrix}
      = 
      \begin{pmatrix}
       \sum_{i=1}^N\dfrac{\tilde{x}_i\tilde{y}_i}{\sigma_i^2} \\
       \sum_{i=1}^N\dfrac{\tilde{y}_i}{\sigma_i^2}
      \end{pmatrix}.
    \end{equation}
    The major difference between this weighted least squares approach and the MAP estimate is that here only the uncertainty in the measurements is taken into account; parameter uncertainty is disregarded. %
    Note that if we had no parameter uncertainty such that $a=a_0$ and $b=b_0$, the MAP method would recover the same system of equations as with linear least squares. 
    
   \end{enumerate}
   
   
  \end{enumerate}


\end{document}

\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{geometry,amsmath,color,graphicx}
\usepackage{verbatim}
\geometry{margin=1in}

%% --- New commands
\newcommand{\pderiv}[3][]{% \pderiv[<order>]{<func>}{<var>} 
  \ensuremath{\frac{\partial^{#1} {#2}}{\partial {#3}^{#1}}}}
  
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\degSym}{$^{\circ}$}
\newcommand{\bigo}[1]{$\mathcal{O}($#1$)$}

%% --- Figures
\graphicspath{ {../figures/} }
\DeclareGraphicsExtensions{.png,.pdf}

% %opening
\title{\vspace{-10ex}Parallel Algorithms \\ HW2}
\author{Timothy Smith}
\date{\vspace{-3ex}}

\pdfinfo{%
   /Title (Parallel Algorithms HW2)
   /Author (Timothy Smith)
}

\begin{document}
\maketitle

\begin{enumerate}

% ---------
%   P1
% ---------
\item The main idea is to do two scans. The first computes the coefficient to
the term containing $x_0$, and the second computes the terms containing $b_i$.
i.e.
\begin{align*}
 &x_0 = x_0 \\
 &x_1 = a_1x_0 + b_1 \\ 
 &x_2 = a_2a_1x_0 + a_2b_1 + b_2 \\
 &x_3 = a_3a_2a_1x_0 + a_3a_2b_1 + a_3b_2 + b_3
\end{align*}

Note: I assume since $a$ is given for $i=1,...,n-1$ that we can set $a(0)=1$.
Similarly $b(0)=0$.

\begin{verbatim}
p1(a, b, x, n)                                       W       D
-------------------------------------------------------------------
% Given x(0) compute for i=1:n-1:
%      x(i) = a(i)*x(i-1) + b(i)
%
% a, b, x: given vectors
% n = size of vector

% 1. Create first coefficient using scan 
% with multiplication instead of addition
a(0) = 1
A = inclusive_scan( a, op=multiplication )         nlog(n) log(n)

% 2. Compute second set of terms with new scan
b(0) = 0
B = new_scan( A, b, n )                            nlog(n) log(n)

% 3. Add it up
parfor i=1:n-1
  x(i) = A(i)*x(0) + B(i)

return x
-------------------------------------------------------------------

new_scan(A, B, n)                                       W       D
-------------------------------------------------------------------
% Scan with new operation
% A = vector with [1, a1, a2*a1, a3*a2*a1,...] 
% B = vector initially containing [0, b1, b2,...]

if (n==1) 
  s(0)=B(0)
  return s(0)

parfor i=0:n/2-1                                                
  Bnew(i) = A(2*i+1)/A(2*i)*B(2*i) + B(2*i+1)           1       1
  Anew(i) = A(2*i+1)                                    1       1

C = new_scan( Anew, Bnew, n/2 )                       W(n/2)  D(n/2)

s(0)=B(0)
parfor i:n-1                                                    
  if isOdd(i)
    s(i) = C(i/2)                                       1       1
  else
    s(i) = A(i)/A(i-1)*C(i/2-1) + B(i)                  1       1

return s
-------------------------------------------------------------------

\end{verbatim}

% ---------
%   P2
% ---------

\item Total work and time complexity, assuming $n\sim m$ is given below.
Note that $p$ is simply a chunking parameter, not a number of parameters.

\begin{align*}
W(n,p) = n + p\log(n) \\
T(n,p) = n/p + \log(n)
\end{align*}

 
\begin{verbatim}
pram_merge(a, b, n, m, tid, p)                          W       D
-------------------------------------------------------------------
% a,b = initial vectors to merge
% n,m = lengths of a and b
% tid = thread id 
% p = a parameter for chunking out the algorithm


r = p/2;
id2 = mod(tid,p/2);

% 1. Chunk a & b into p/2 chunks, grab p/2-1 splitters each
%    and compute rank in opposite vector

if tid < p/2
  rAinB[id2] = rank( A[id2*n/r], B );                 plog(m)  log(m)
  rAinC[id2] = id2*n/r + rAinB[id2];                    1       1

else 
  rBinA[id2] = rank( B[id2*m/r], A );                 plog(n)  log(n)
  rBinC[id2] = id2*m/r + rBinA[id2];                    1       1
end

% 2. Sequential merge sorted arrays to global splitters in all vectors
%    Split work over 3 threads b/c why not
%    Assume seq_merge ends with a GLOBAL_WRITE

if tid==0 
  sC = seq_merge( rAinC, rBinC );                       p       p
end
if tid==1
  sA = seq_merge( [1:r]*id2*n/r, rBinA );               p       p
end
if tid==2
  sB = seq_merge( [1:r]*id2*m/r, rAinB );               p       p
end

% 3. Now merge each chunk into C
if tid==0
  C[0:sC[tid]] = seq_merge( ...                         n      n/p 
        A[0:sA[tid]],B[0:sB[tid]] );                    
else
  C[sC[tid-1]:sC[tid]] = seq_merge( ...                 n      n/p 
        A[sA[tid-1]:sA[tid]],B[sB[tid-1]:sB[tid]] );
end
  
return C;

-------------------------------------------------------------------
\end{verbatim}
        
% ---------
%   P3
% ---------

\item Total work and depth:

\begin{align*}
W(n,p) &= n(\log(n/p))^2 + p^2(\log(p))^2 + n\log(n/p) \\
       &= n\log(n/p)(1+\log(n/p)) + p^2(\log(p))^2  \\ 
D(n,p) &=  (\log(n/p))^2 + (\log(p))^2 + \log(p) + \log(n/p) \\
       &=  (\log(n/p))^2 + (\log(p))^2 + \log(n) \\
T(n,p) &= n/p\log(n/p) + (n/p+1)(\log(n/p))^2 + (p+1)(\log(p))^2 + \log(n) \\
       &\sim n/p\log(n/p)(1+\log(n/p)) + p(\log(p))^2 
\end{align*}
assuming $n >> p$.

Details on work / depth calculations:
\begin{enumerate}
  \item Step 1. Bitonic sort over each chunk (use bitonic sort because I use it
later with splitters). The depth arises from making the list bitonic, and is:
        \begin{align*}
          D(n) &= \sum_{i=1}^{\log(n)} i \\
               &= (\log(n))^2 \; .
        \end{align*}
and the work is simply: $W(n,p) = n*D(n/p)$, since here the depth is governed by
how the array is chunked out.

  \item Step 2. Assigning splitters is free since this just amounts to grabbing
elements in a sorted array.
        
  \item Step 3. Bitonic sort over all splitters (use bitonic sort to reduce
complexity for flexible $p$). The depth is computed above, here it is governed
by the $p^2$ splitters so is: $D(p^2) \sim (\log(p))^2$. The work is then
$W(p^2) = p^2(\log(p))^2$, which is split over $p$ processes. 

  \item Step 4. Assigning keys to buckets will have $D(p)=\log(p)$ for a standard
divide and conquer approach ($p$ governs depth since we do this for $p$
splitters). Since we have to touch each element to figure out
what bucket it goes to, the work is $W(n,p) = n\log(p)$. Once the buckets are assigned,
bucket\_sort has $D(n,p) = \log(n/p)$, which since the depth is computed over $n/p$
elements in each chunk. This is done $n$ times for work: $W(n)=n \log(n/p)$.
\end{enumerate}

\begin{verbatim}
sample_sort(a, n, p)                                  W       D
-------------------------------------------------------------------
% a = initial vector
% n = size of vector
% p = a parameter for chunking out the algorithm

r=n/p

% 1. Locally sort chunks
parfor i = 0:n-1 
  b=i*r 
  e=(i+1)*r - 1
  c(b:e) = bitonic_sort( a(b:e), r )              n(log(n/p))^2
                                                          (log(n/p))^2 
  
% 2. Locally assign p-1 splitters on e/ chunk
parfor i = 0:p-1
  b=i*(p-1)
  e=(i+1)*(p-1)-1
  s(b:e) = assign_splitters( c(b:e), p-1 )            1       1

% 3. Bitonic sort splitters, grab global splitters
s = bitonic_sort( s, p*(p-1) )                    p^2(log(p))^2
                                                          (log(p))^2
gs= assign_splitters( s, p-1 )                        1        1

% 4. Assign keys to buckets and bucket sort
parfor i = 0:p-1
  b=i*r
  e=(i+1)*r - 1
  k(b:e) = key_to_bucket( c(b:e), r )                 n     log(p) 

c = bucket_sort( c, k, n, p )                      n(log(n/p))     
                                                            log(n/p)
                                                         
return c                                                   -
-------------------------------------------------------------------

\end{verbatim}

% ---------
%   P4
% ---------

\item Work complexity is: 

\begin{align*}
W(n) &= 2n + W(n/5) + W(7n/10) \\
     &\leq 2n + W(9n/10) \\
     &= 2n(1 + (9/10) + (9/10)^2 + ...) \\
     &= \mathcal{O}(n)  
\end{align*}

So the algorithm is work optimal

Depth is:
\begin{equation*}
D(n) = \log(n) + D(n/5) + D(7n/10)
\end{equation*}

The Akra-Bazzi theorem states
\begin{align*}
T(n) &= g(n) + \sum_{i=1}^k a_iT(b_i n + h_i(x)) \\
     &= \mathcal{O}(n^p(1 + \int_1^n \frac{g(x)}{x^{p+1}} \, dx ))
\end{align*}

where $p$ is s.t. $\sum_{i=1}^ka_i (b_i)^p = 1$.

Here $g(n) = log(n)$, $k=2$, $a_1=\frac{1}{5}$, $a_2=\frac{7}{10}$, $h_i(n) = 0
\, \forall i$, $p\simeq 0.84$ and the depth is: 

\begin{align*}
D(n) &= \log(n) + D(n/5) + D(7n/10) \\
     &= \mathcal{O}\left(n^p\left(1 + \int_1^n \frac{log(x)}{x^{p+1}} \, dx
\right)\right) \\
     &= \mathcal{O}\left(n^p\left(1 + \int_0^{\log(n)} e^{-p u} \, du
\right)\right) 
\end{align*}

where we make the substitution $u = log(x), du = 1/x dx$. 

\begin{align*}
     &= \mathcal{O}\left(n^p\left(1 +
\left[-\frac{e^{-pu}}{p}\right]_{0}^{\log(n)}\right)\right) \\
     &= \mathcal{O}\left(n^p\left(1 + \left[-\frac{n^{-p}}{p} +
\frac{1}{p}\right]\right)\right) \\
     &= \mathcal{O}\left(n^p\left(1+\frac{1}{p}\right) - \frac{1}{p} \right)  \\
     &\sim \mathcal{O}\left(n^{0.84}\right)
\end{align*}
     
which is less than \bigo{$n$}.

\begin{verbatim}
parallel_median(a, n, k)                                W       D
-------------------------------------------------------------------
% a = original vector
% n = length of a
% k = element of vector to grab 

if n<=5
  m = seq_median( a, n );                              O(1)    O(1)
  return m

% 1. Chunk into g groups, 5 long, sequentially find median
g=n/5;
parfor i=0:g-1
  med(i) = seq_median( a(i*5:(i+1)*5-1), 5 );          

% 2. Recursively find median of medians
p = par_select(med, g, n/2);                          W(n/5)  D(n/5)

% 3. Use select (scan) to get elements less than pivot
%    Do inverse to get elements greater than pivot
L = select(a, n, p);                                   n      log(n)
G = inverse_select(a, n, p);                           n      log(n)

% 4. Test size of L to recurse or return median
Nl = length(L);
if Nl == k-1
  m = p;
  return m
elseif Nl > k-1
  m = parallel_median(L, Nl, k);                     W(7n/10) D(7n/10) 
elseif Nl < k-1
  m = parallel_median(G, length(G), k-Nl-1);         W(7n/10) D(7n/10)

return m

-------------------------------------------------------------------

\end{verbatim}


\end{enumerate}
\end{document}

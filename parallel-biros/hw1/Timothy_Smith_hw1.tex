\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{geometry,amsmath,color,graphicx}
\usepackage{verbatim}
\geometry{margin=1in}

%% --- New commands
\newcommand{\pderiv}[3][]{% \pderiv[<order>]{<func>}{<var>} 
  \ensuremath{\frac{\partial^{#1} {#2}}{\partial {#3}^{#1}}}}
  
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\degSym}{$^{\circ}$}
\newcommand{\bigo}{$\mathcal{O}$}

%% --- Figures
\graphicspath{ {../figures/} }
\DeclareGraphicsExtensions{.png,.pdf}

% %opening
\title{\vspace{-10ex}Parallel Algorithms \\ HW1}
\author{Timothy Smith}
\date{\vspace{-3ex}}

\pdfinfo{%
   /Title (Parallel Algorithms HW1)
   /Author (Timothy Smith)
}

\begin{document}
\maketitle

\begin{enumerate}

% ---------
%   P1
% ---------
\item
        Reading. 


% ---------
%   P2
% ---------

\item All elements of the outer product can be computed independently, so this is conveniently parallel.  \\  

\begin{center}
Work = \bigo$(n^2)$ \\
Depth = \bigo(1) \\
Parallelism = \bigo$(n^2)$ \\
\end{center}

\begin{verbatim}
outer_prod(x,y,n)                                 W     D
-------------------------------------------------------------------
\\ x = n x 1 vector
\\ y = 1 x n vector
\\ n = length of vectors

parfor k = 0:n^2-1
  i = floor(k/n)                                  1
  j = mod(k,n)                                    1     1 
  A(i,j) = x(i)*y(j)                             n^2    

return A
-------------------------------------------------------------------
\end{verbatim}

        
% ---------
%   P3
% ---------

\item A PRAM Algorithm for reduction is given below, and is written generally in MATLAB notation.  The general idea is as follows: compute the number of levels for computing a reduction, where if mod($p$,2) $\neq 0$ then we round up. Rather than recursing over these levels, I loop over them (\texttt{reduction} steps 2-5). In the initial step the vector is split up over $p$ threads and each thread computes its local sum via the subroutine \texttt{doAdd}. Then in each following level (corresponding to the loop counter in step 2 of \texttt{reduction}) $2^{i-1}$ threads are used to compute the next level's sum.

Note: Work given in algorithm below is the work per process while work described here is the total work as defined in class.
\begin{center}
Work: \bigo($n + \log(p)$) \\
Time Complexity: \bigo($n/p + \log(p)$) \\
Speedup: $\frac{n}{\frac{n}{p} + \log(p)} = \frac{1}{\frac{1}{p} + \frac{1}{n}\log(p)}$ \\
Efficiency: $\frac{1}{1+\frac{p}{n}\log(p)}$
\end{center}

The algorithm is not work efficient but as $n$ grows large efficiency goes to 1.

\begin{verbatim}
pram_reduction(a, p, tid)                                W       D
-------------------------------------------------------------------
\\ a = initial vector
\\ p = number of threads to share work over
\\ tid = thread id

1. L = ceil( log( p ))                                  O(1)    O(1)
2. for i = L:-1:1                                             -   
3.     if i==L, plim=p; else, plim=2^(i-1)              O(1)   |  
4.     n = length(a)                                    O(1)   |-O(log(p))
5.     a = doAdd(a,n,plim,tid)                        O(n/plim)|
6. return a                                                   -
-------------------------------------------------------------------


doAdd(v,m,q,tid)                                         W       D
-------------------------------------------------------------------
\\ v = vector of length m
\\ m = length of v
\\ q = number of threads to share work over
\\ tid = thread id

1. m = length(v)                                        O(1)    O(1)
2. r = m/q                                              O(1)    O(1)
3. s = zeros(q,1)                                       O(1)    O(1)
4. b = tid*r                                            O(1)    O(1)
5. e = tid*r+r-1                                        O(1)    O(1)
6. w = sum( a(b:e) ) // local sequential sum           O(m/r)   O(1)
7. s(tid) = GLOBAL_WRITE( w )                           O(1)    O(1)
8. return s                                             O(1)    O(1)
-------------------------------------------------------------------

\end{verbatim}

% ---------
%   P4
% ---------

\item Work complexity is: 
\begin{equation}
        W(n,p) = \mathcal{O}(n^2)
\end{equation}

so the algorithm is work efficient.

\begin{equation}
        T(n,p) = \mathcal{O}(\frac{n^2}{p^2} + \frac{n}{p}\log(p))
\end{equation}

Biggest time cost is from $n^2$ operations from sequential matrix vector multiply, shared over $p^2$ processors. $\frac{n}{p}\log(p)$ cost comes from depth of reduction, which happens $n/p$ times across $p$ processors.

Speedup is:

\begin{equation}
\begin{split}
        S(n,p) &= T_s/T_p  \\
               &= \frac{n^2}{\frac{n^2}{p^2} + \frac{n}{p}\log(p)} \\
               &= \frac{1}{\frac{1}{p^2} + \frac{1}{np}\log(p)}
\end{split}
\end{equation}

Thus with large enough $n$ speedup approaches $p^2$, where the reduction in $p$ causes this to decrease proportional to $1/\log(p)$. 

\begin{verbatim}
pram_matvec2d(A, x, n, p, tid)                            W     D
-------------------------------------------------------------------
\\ A = matrix, nxn
\\ x = nx1 vector
\\ n = length of x
\\ p = square root of number of processors to share work over
\\     Assuming we have a pxp grid of cores
\\ tid = thread id

r = n/p                                                   1     1
rowID = floor(tid/r)                                      1     1
colID = mod(tid,r)                                        1     1
bi = rowID*r                                              1     1
bj = colID*r                                              1     1
ei = bi+r-1                                               2     1
ej = bj+r-1                                               2     1

z = GLOBAL_READ(x[bj:ej])                                 n     1
B = GLOBAL_READ(A[bi:ei,bj:ej])                           n     1
w = sequential_matvec(B,z,r)                             n^2    1

v = zeros(r,1)
for k = 1:r
  v[k] = pram_reduction(w[k],p,colID)                     n   n/plog(p)
end

y(bi:bj) = GLOBAL_WRITE( v )                              n     1
-------------------------------------------------------------------


sequential_matvec(A, x, n)                                W      D
-------------------------------------------------------------------
\\ A = matrix, nxn
\\ x = nx1 vector
\\ n = length of x
\\ p = number of processors to share work over
\\ tid = thread id

y = zeros(n,1)

for i=1:n
  for j=1:n
    y[i] = y[i] + A[i,j]*x[j]                             n^2    n
  end
end
-------------------------------------------------------------------

subroutine pram_reduction: see problem (3)

\end{verbatim}


% ---------
%   P5
% ---------

\item Derivation of work and depth are shown for the first couple iterations over $i$ in Figure \ref{fig:q5}. Both work and depth for this algorithm are \bigo($n^2$). A PRAM model is given below. Here I parallelized the element wise multiplication and summation $\sum_j A_{ij}x_j$. Time complexity is:

\begin{equation}
        T(n,p) = \mathcal{O}(\frac{n^2}{p} + n \log(n))
\end{equation}

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{problem5}
  \label{fig:q5}
\end{figure}


\begin{verbatim}
pram_lowtri(A, b, n, p, tid)                              W      D
-------------------------------------------------------------------
\\ A = matrix in Ax=b, nxn
\\ b = RHS of eqn
\\ n = length of b
\\ p = number of threads to share work over
\\ tid = thread id

if n==1
  return b[0]/A[0,0]                                      1      1
else
  x[0:n-2] = pram_lowtri( A[0:n-2,0:n-2], b[0:n-2], n-1, p, tid )

                                                        W(n-1) D(n-1)
                                                          
 
  r = n/p                                                 1      1
  b = r*tid                                               1      1
  e = r*tid + r - 1                                       1      1
  w = A[n-1,b:e] .* x[b:e]                                n     n/p
  s(b:e) = GLOBAL_WRITE(w)                                n      1

  x[n-1] = pram_reduction(s,n,p,tid)                      n    log(n)
  
  x[n-1] = b[n-1] - x[n-1]                                1      1
  x[n-1] = x[n-1] / A[n-1,n-1]                            1      1

  return x
end
-------------------------------------------------------------------

subroutine pram_reduction: see problem (3)

\end{verbatim}

% ---------
%   P6
% ---------

\item This algorithm only changes slightly from one where mod($p$,2)$=0$. Here $2^{d-1} < p < 2^d$, and we want to map the ``excess'' processors (i.e. excess = $p-2^{d-1}$) to elements on a $d$ dimensional hypercube. Then when running through the algorithm we just want to make sure that there is a partner for the sending and receiving (i.e. partner $<= p$).

\begin{equation}
        T(n,p) = \mathcal{O}(n/p + ceil(\log(p))t_h + t_w)
\end{equation}

Here the first term arises from local sums (i.e. where $n>p$ so processors need to first reduce their local chunk). The second arises from latency, and $t_h$ is the latency time. The last term is from message passing after latency is overcome, where $t_w$ is the inverse of bandwidth and message size is \bigo(1). 

\begin{verbatim}
hcube_reduction_genericp(a, p, tid)                       W      D
-------------------------------------------------------------------
// a = initial data
// p = number of threads to share work over
// tid = thread id

// first if n>p, collapse local data
r=n/p                                                     1      1
if r>1
  s = zeros(p,1)                                          p      1
  b = tid*r                                               1      1
  e = tid*r + r -1                                        1      1
  w = sum(a(b:e))                                         n      1
  s(tid) = GLOBAL_WRITE(w)                                p      1
else
  s = a                                                   1      1
end
  
// Now deal with communicating 
d = ceil(log2(p)) // map to higher dim hcube            O(1)    O(1)
mask = 0                                                  1   
for k = 0:d-1                                                - 
  r=2^k                                                   1   | 
                                                              |
  if tid & mask == 0                                          |
    partner = xor(tid,r)                                  1   |
    if partner < p // only proceed if partner exists          |
      if tid & r                                              |- O(log(p))
        send(s,partner)                                   p   | 
      else                                                    |
        recv(sp,partner)                                  p   |
        s=s+sp                                            p   |
      end                                                     |
    end                                                       |  
  end                                                         |
                                                              |
  mask = xor(mask,r)                                      1   |
end                                                          -
return s
-------------------------------------------------------------------
\end{verbatim}

\end{enumerate}
\end{document}

\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}

% --- Heading
\title{\vspace{-4ex} Satellite Position Estimation \\ ASE 381P Semester Project}
\author{Tim Smith}
\date{\vspace{-3ex}}

% --- Packages
\usepackage{geometry,amsmath,color,graphicx,listings}
\geometry{margin=1in}
\begin{document}

%% --- New commands
\newcommand{\pderiv}[3][]{% \pderiv[<order>]{<func>}{<var>} 
  \ensuremath{\frac{\partial^{#1} {#2}}{\partial {#3}^{#1}}}}
\newcommand{\red}[1]{\textcolor{red}{#1}}

\lstset{language=Matlab,
  basicstyle=\footnotesize}

\maketitle

\section{Comparison of Batch, Sequential, and Extended Sequential Estimators}
\label{original}

  The state deviation vectors for each of the estimators are shown in Figure \ref{fig:state-dev}, and their covariance matrices in Figures \ref{fig:covariance1} - \ref{fig:covariance3}. After two iterations through the data, the batch and sequential estimators produce roughly the same estimate of the state deviation. Considering this vector has values which are roughly ten orders of magnitude smaller than their corresponding state values, it is apparent that the filters have converged to a state estimate. All of the estimates here are roughly the same order of magnitude, other than those for $\mu$ and $\beta$. The batch estimator produces an estimate of $\mu$ to be $10^1$, which seems unrealistically small given that the value for the state is $O(10^{14})$. Also, the previous two estimates are on the order of the sequential estimate ($10^5$). I take this difference to be due to fluctuations under resolved numerical precision in MATLAB's ODE45 time integration routine, because the state deviation seems to be within the noise of values which are beyond its relative tolerance (which I set to $O(10^{-10})$). There is a similar discrepancy in $\beta$, which I presume to be due to the same effects. The covariance matrix between the two is identical, to within four decimal places. It seems strange that there is discrepancy between the two state deviation vectors by four orders of magnitude for only two parameters but the covariance matrices are almost identical. My hypothesis here is still that this discrepancy (or lack thereof in the covariance matrix) is due to precision in numerical integration. \\

  The extended sequential algorithm produces qualitatively different results than the other two estimators. The state deviation vector converges after the first pass through the data (i.e. iteration zero), and subsequent iterations produce virtually no changes on the results. Interestingly though, the solution from the batch and sequential estimates produce deviation vectors with slightly smaller estimates - by roughly an order of magnitude in some cases. The covariance matrix is almost identical between the three, though. \\

  The Root Mean Squared (RMS) error for the observation error: 
    \begin{equation}
     \sqrt{\frac{1}{N}\sum_{i=0}^Ny(t_i)^2}     
    \end{equation}
  where $y(t_i) = Y(t_i) - G(X(t_i))$ is plotted in Figure \ref{fig:rmsObs}. The solutions for batch and sequential estimators are identical, converging to $10^{-1}$ after three full passes through the observation data. The extended sequential algorithm shows no improvement after the first pass, which makes sense because its estimate of the state does not change after the first pass (and even this update is marginal). Interestingly again, the RMS error for the Extended sequential algorithm is roughly four times that of the batch and sequential. 

	\begin{figure}
	\centering
	\includegraphics[width=.6\textwidth]{rmsObs.png}
	\caption{RMS error in the observation residuals is shown for each estimation algorithm. Note that the sequential and batch algorithms produce identical results, and so the points lie on top of one another.}
	\label{fig:rmsObs}
	\end{figure}

\section{Batch Revisited: Minimal Initial Station Error}
\label{prob5}

	After running the batch algorithm with tiny error ($\sigma^2 = 10^{-10} m^2$) for the estimate in position for station 1, the results are quite different. Here the state deviation vector seems to have converged, with values of similar order to those estimated in the previous step other than those for station 1, which are notably smaller. The covariance matrix shows a similar structure:
	\begin{enumerate}
	  \item All of the values in the first six columns and rows are generally the same order. 
	  \item The covariance for $\mu$ is significantly higher in this case.
	  \item The eighth and ninth columns and rows are generally the same. 
	  \item The covariance entries for the the last three columns and rows in this case are much smaller than before, indicating that this estimate is very close to the observations. This is due to the inflated weighting from the initially very small \textit{a priori} variance estimate. 
	\end{enumerate}

	The problem here is that the filter never actually converges to the correct solution - the RMS error of the observation residuals never gets below $10^3$. The filter cannot converge to the true solution because it is given the same prior estimate of the state as the batch run in the original case, but is given the false information that these values are nearly exact (through extremely small variance estimates). With these tiny estimates of variance, the filter does not change its estimate of the state for station one and the error in observations can never converge. 

\section{Batch Revisited... again: Minimal error with the correct state}
\label{prob6}

	In another run with the batch algorithm, we estimate with the prior uncertainty given in the last section but this time feed the filter a prior state estimate given by the final iteration through the data in the original scenario. This time the filter does much better: the RMS error of the observation residuals has converged after a single pass through the data. We would expect this immediate convergence since the state given to the filter as prior information is very close to the observations already, so we would not expect much change here. The state deviation vector becomes further refined from the original estimate (i.e. Section \ref{original}), and on the third pass through the data zeros are returned for the vector and covariance. The fact that zeros are returned seems to be an artefact of numerical precision, since the state deviation vector values for station 1 are reported as $O(10^{-16})$ after two passes through the data. 
	
	\begin{figure}[hb]
	\centering
	\includegraphics[trim={0 3.5cm 0 3cm},clip,width=\textwidth]{state-dev.pdf}
	\caption{State deviation vectors from each of the estimation schemes.}
	\label{fig:state-dev}
	\end{figure}
	
	\begin{figure}
	\centering
	\includegraphics[trim={0 4cm 0 3cm},clip,width=\textwidth]{covariance-matrices-1.pdf}
	\caption{Covariance matrices from the batch and sequential estimation schemes. Each matrix is after the final iteration of processing the observation data, corresponding to the ``iter 2'' column in the state deviation vectors.} 
	\label{fig:covariance1}
	\end{figure}

	\begin{figure}
	\centering
	\includegraphics[trim={0 4cm 0 2.5cm},clip,width=\textwidth]{covariance-matrices-2.pdf}
	\caption{Covariance matrices from the extended sequential and batch estimation schemes, where the batch matrix here corresponds to the scenario discussed in Section \ref{prob5}. Each matrix is after the final iteration of processing the observation data, corresponding to the ``iter 2'' column in the state deviation vectors.} 
	\label{fig:covariance2}
	\end{figure}
	
	\begin{figure}
	\centering
	\includegraphics[trim={0 11cm 0 3cm},clip,width=\textwidth]{covariance-matrices-3.pdf}
	\caption{Covariance matrix from the batch estimation scheme, corresponding to the scenario discussed in Section \ref{prob6}. The matrix corresponds to the state deviation vector with iter 1 since after the third time through the data, MATLAB recognizes the state deviation as zero since some of the values go below machine precision. This causes the final covariance matrix to be zeros.} 
	\label{fig:covariance3}
	\end{figure}
	
	\newpage
	\section{Matlab code}
	
	 \subsection{Batch Estimator}
	 \lstinputlisting{../src/runBatch.m}
	 
	 \subsection{Sequential (\& Extential) Estimator)}
	 \lstinputlisting{../src/runSequential.m}
	 
	 \subsection{Compute Observation Residuals and $\tilde{H}$}
	 \lstinputlisting{../src/bigGandH.m}
	 
	 \subsection{Time integration}
	 \lstinputlisting{../src/integrateInTime.m}
	 
	 \subsection{Partial Differential Equations: $\dot{X}$ and $\dot{\phi}$}
	 \lstinputlisting{../src/xAndPhiFunc.m}
	 
	 \subsection{Packing algorithms}
	 \lstinputlisting{../src/packForPhi.m}
	 \lstinputlisting{../src/packForPhiDot.m}
	 \lstinputlisting{../src/unpackForPhi.m}
	 \lstinputlisting{../src/unpackForPhiDot.m}
	 
	 \subsection{Read observation file}
	 \lstinputlisting{../src/readObs.m}
	 
	 \subsection{Set Initial Conditions and Constants}
	 \lstinputlisting{../src/setInitialConditions.m}
	 \lstinputlisting{../src/setConstants.m}
\end{document}

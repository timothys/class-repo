\documentclass[article,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{geometry,amsmath,color,graphicx}
\usepackage{verbatim}
\usepackage{subcaption}
\usepackage{listings}
\geometry{margin=1in}

%% --- New commands
\newcommand{\pderiv}[3][]{% \pderiv[<order>]{<func>}{<var>} 
  \ensuremath{\frac{\partial^{#1} {#2}}{\partial {#3}^{#1}}}}
  
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\degSym}{$^{\circ}$}
\newcommand{\bigo}{$\mathcal{O}$}

%% --- Figures
\graphicspath{ {../figures/} }
\DeclareGraphicsExtensions{.png,.pdf}

% %opening
\title{\vspace{-10ex}Inverse Methods \\ HW3}
\author{Timothy Smith}
\date{\vspace{-3ex}}

\pdfinfo{%
   /Title (Inverse Methods HW3)
   /Author (Timothy Smith)
}

\begin{document}
%\maketitle

\begin{enumerate}
\item (on paper)
% ---------
%   P3
% ---------
\item
\begin{enumerate}
 \item (on paper)
 \item The results from solving the Poisson problem are shown in Fig.
\ref{fig:poisson}. The impact of the anisotropic conductivity tensor on
the solution is clear, showing the discrepancy in curvature in the direction of
the two eigenvectors.

\begin{figure}
 \centering
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=.75\linewidth]{plt_hw3_2_1}
  \caption{$\mathbf{A} = \mathbf{A_1}$, isotropic}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_hw3_2_2}
  \caption{$\mathbf{A} = \mathbf{A_2}$, anisotropic}
 \end{subfigure}
 \caption{Solution to the Poisson problem are shown for (left) an
isotropic conductivity tensor and (right) a highly anisotropic
conductivity tensor.}
 \label{fig:poisson}
\end{figure}
\end{enumerate}

% ---------
%   P3
% ---------
\item

\begin{enumerate}
% --- Part A
\item Solving the image reconstruction with Tikhonov regularization is straight
forward mechanically since the problem is linear. The solution is shown for
various regularization parameter values: $k(\mathbf{x})=\alpha$ in Fig.
\ref{fig:findAlphaTikh}. The value which balances smoothness with detail is
found to be $\sim 10^{-2}$. While this solution is not optimal, all of the
highly oscillatory noise which is present in the solution at $10^{-4}$ and
$10^{-3}$ has been removed. To some degree, the edges of the longhorn are
present which are completely smoothed out with $\alpha = 10^{-1}$. 


% --- Part B
\item First we seek the proper value for $k(\mathbf{x})=\alpha$, i.e. the regularization
parameter. Setting $\epsilon=10^{-2}$ (the middle of the suggested range), I
varied $\alpha \in [10^{-4}, 10^{-1}]$, as shown in Fig. \ref{fig:findAlpha}.
Clearly, from the values tested, $\alpha \simeq 10^{-2}$ is the most reasonable
choice: providing a solution which is mostly noiseless but maintains the desired
sharp edges along the shape of the longhorn (i.e. smooth but not too
smooth). With $\alpha = 10^{-1}$ the solution is clearly too smooth as the left
horn is nearly erased: a solution that only an Aggie would find satisfactory.
Clearly there is too much noise with $\alpha \leq 10^{-3}$ as the solution shows
rough, highly oscillatory patterns.

Next, $\epsilon$ was varied from $10^{-4}$ to $1$, and the results are shown in
Fig. \ref{fig:findEps}. The impact on tuning $\epsilon$ is much more subtle than
with $\alpha$, as the image is clear with relatively well preserved edges across
all values. However, with higher values of $\epsilon$ the regularization begins
to look smoother, and similar to Tikhonov since near $|\nabla u| = 0$, $(\nabla u \cdot \nabla
u + \epsilon)^{1/2}$ looks quadratic. As $\epsilon$ is decreased the number of
nonlinear iterations in the
solver increased dramatically. Using the same tolerances provided in the script
\texttt{EnergyMinimization.ipynb}, the number of iterations were 1, 130, 168,
and 1000 for $\epsilon = [\geq 10^{-1}, 10^{-2}, 10^{-3}, 10^{-4}]$. Note that
at $\epsilon \geq 10^{-1}$ the solver terminated early due to reaching the
maximum number of backtracking iterations. Despite tuning the line search
parameters, no solution showed improvement over the results after 1 nonlinear
iteration. Also note that at $\epsilon = 10^{-4}$ the solver terminated early at
1000 nonlinear iterations. The reason for the dramatic increase in nonlinear
solves as $\epsilon$ decreased is because of the singularity in the Hessian
which is approached
when $\epsilon$ is near zero and the search direction has components in the
direction of $\nabla u$. Here, as shown in \#1b, the regularization starts
approaching the limit to where it is no longer differentiable, and computing the
Newton step becomes ill conditioned.


% --- Part C
\item The true and noisy images are shown along side the ``best''
reconstructions from Tikhonov and Total Variation regularization in Fig.
\ref{fig:finalComparison}. Clearly total variation preserves edges much better
than Tikhonov. 

\begin{figure}[hb]
 \centering
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m4_tn}
  \caption{$\alpha = 10^{-4}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m3_tn}
  \caption{$\alpha = 10^{-3}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m2_tn}
  \caption{$\alpha = 10^{-2}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m1_tn}
  \caption{$\alpha = 10^{-1}$}
 \end{subfigure}
 \caption{Results from varying alpha from $10^{-4}$ to $10^{-1}$ using Tikhonov
regularization. $\alpha \geq 10^{-1}$ are clearly over-regularized, while
$\alpha \leq 10^{-3}$ are under-regularized, and quite noisy.} 
 \label{fig:findAlphaTikh}
\end{figure}

\begin{figure}[ht]
 \centering
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m4}
  \caption{$\alpha = 10^{-4}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m3}
  \caption{$\alpha = 10^{-3}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m2}
  \caption{$\alpha = 10^{-2}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m1}
  \caption{$\alpha = 10^{-1}$}
 \end{subfigure}
 \caption{Results from varying alpha from $10^{-4}$ to $10^{-1}$ using total
variation regularization. Here $\epsilon = 0.01$ since was in the middle of the
suggested range of values. $\alpha \geq 10^{-1}$ are clearly over-regularized, while
$\alpha \leq 10^{-3}$ are under-regularized, and quite noisy.} 
 \label{fig:findAlpha}
\end{figure}


\begin{figure}[ht]
 \centering
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m4}
  \caption{$\alpha = 10^{-4}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m3}
  \caption{$\alpha = 10^{-3}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m2}
  \caption{$\alpha = 10^{-2}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m1}
  \caption{$\alpha = 10^{-1}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m0}
  \caption{$\alpha = 1$}
 \end{subfigure}
 \caption{Results from varying alpha from $10^{-4}$ to $10^{-1}$ using total
variation regularization. Here $\alpha = 0.01$, as indicated from the Fig.
\ref{fig:findAlpha}.} 
 \label{fig:findEps}
\end{figure}



\begin{figure}[ht]
 \centering
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_u_true}
  \caption{Original, ``true'', image}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_u_0}
  \caption{Image with noise (i.e. $u_0$)}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_alpha10m2_tn}
  \caption{\centering ``Best'' Tikhonov reconstruction with $\alpha=10^{-2}$}
 \end{subfigure}
 \begin{subfigure}{0.45\textwidth}
  \includegraphics[width=\linewidth]{plt_eps10m1}
  \caption{\centering ``Best'' Total Variation reconstruction with $\alpha=10^{-2}$ and
$\epsilon=10^{-1}$}
 \end{subfigure}
 \caption{Final comparison of image reconstruction using Tikhonov and Total
Variation regularization. Clearly total variation preserves edges much better.} 
 \label{fig:finalComparison}
\end{figure}
\end{enumerate}

\end{enumerate}

\clearpage
\newpage
\section{Problem 2 code}
\lstinputlisting[language=python]{../src/hw3_2.py}
\section{Problem 3 code}
\lstinputlisting[language=python]{../src/hw3_3.py}

\end{document}

import matplotlib.pyplot as plt
#%matplotlib inline

from dolfin import *
import math
import numpy as np
import logging
from unconstrainedMinimization import InexactNewtonCG

logging.getLogger('FFC').setLevel(logging.WARNING)
logging.getLogger('UFL').setLevel(logging.WARNING)
set_log_active(False)


# Set the level of noise:
noise_std_dev = .3

# Load the image from file
data = np.loadtxt('image.dat', delimiter=',')
np.random.seed(seed=1)
noise = noise_std_dev*np.random.randn(data.shape[0], data.shape[1])

# Set up the domain and the finite element space.
Lx = float(data.shape[1])/float(data.shape[0])
Ly = 1.

mesh = RectangleMesh(Point(0,0),Point(Lx,Ly),200, 100)
V = FunctionSpace(mesh, "Lagrange",1)

# Generate the true image (u_true) and the noisy data (u_0)
class Image(Expression):
    def __init__(self, Lx, Ly, data, **kwargs):
        self.data = data
        self.hx = Lx/float(data.shape[1]-1)
        self.hy = Ly/float(data.shape[0]-1)
        
    def eval(self, values, x):
        j = int(math.floor(x[0]/self.hx))
        i = int(math.floor(x[1]/self.hy))
        values[0] = self.data[i,j]

trueImage = Image(Lx,Ly,data, degree=1)
noisyImage = Image(Lx,Ly,data+noise, degree=1)
u_true  = interpolate(trueImage, V)
u_0 = interpolate(noisyImage, V)

plot(u_true,interactive=True)
plot(u_0,interactive=True)

# --- (a) solve with Tikhonov regularization

# Define the finite element space
u = TrialFunction(V)
u_hat = TestFunction(V)

# Set up linear system
for i in range(-2,-1):
        alpha = Constant(10**i)

        
        grad_tn = u*u_hat*dx + alpha*inner(grad(u), grad(u_hat))*dx
        L = u_0*u_hat*dx
            
        uh=Function(V)
        solve(grad_tn == L, uh)
        A, b = assemble_system(grad_tn,L)
        uh=Function(V)
        solve(A,uh.vector(),b)
        
        plot(uh,interactive=True)

# --- (b) solve the big guy
alpha = Constant(0.01)
tv_eps = Constant(0.01)
u = Function(V)
u_hat = TestFunction(V)
u_tilde = TrialFunction(V)

for i in range(-1,1):
        tv_eps = Constant(10**i)
        E_tv = Constant(0.5)*pow(u-u_0,2.)*dx + \
               Constant(0.5)*alpha*pow(inner(grad(u), grad(u))+tv_eps,0.5)*dx 
        
        grad_tv = u_hat*(u-u_0)*dx + \
                  alpha*pow((inner(grad(u),grad(u)) + tv_eps), -0.5)*\
                  inner(grad(u),grad(u_hat))*dx 
        
        H_tv = u_tilde*u_hat*dx - \
               alpha*pow((inner(grad(u),grad(u)) + tv_eps), -1.5)*\
               inner(grad(u),grad(u_hat))*inner(grad(u),grad(u_tilde))*dx + \
               alpha*pow((inner(grad(u),grad(u)) + tv_eps), -0.5)*\
               inner(grad(u_tilde),grad(u_hat))*dx 
        
        solver = InexactNewtonCG()
        solver.parameters["rel_tolerance"] = 1e-6
        solver.parameters["abs_tolerance"] = 1e-9
        solver.parameters["gdu_tolerance"] = 1e-18
        solver.parameters["max_iter"] = 500
        solver.parameters["c_armijo"] = 1e-5
        solver.parameters["print_level"] = 1
        solver.parameters["max_backtracking_iter"] = 10
        
        solver.solve(E_tv, u, grad_tv, H_tv)
        
        plot(u,interactive=True)
        plt.show()

# demo from fenicsproject.org for solving poisson equation

from dolfin import *
import matplotlib.pyplot as plt
import mshr

n=40
d=1
# Create mesh and define function space
mesh = mshr.generate_mesh(mshr.Circle(Point(0.,0.), 1.), n)
Vh = FunctionSpace(mesh, "Lagrange", d)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x,on_boundary):
    r = (pow(x[0],2) + pow(x[1],2))
    tol = 1.0 - DOLFIN_EPS
    return r  < tol and on_boundary

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(Vh, u0, boundary)

# Define variational problem
u = TrialFunction(Vh)
v = TestFunction(Vh)
f = Expression("exp(-100*(pow(x[0], 2) + pow(x[1], 2)))", degree=d+2)

# Define the two conductivity matrices
A1 = Constant( ( (10., 0.), (0., 10.) ) )
A2 = Constant( ( (1., -5.), (-5.,100.) ) )

# Assemble and solve
a = inner(A2*grad(u), grad(v))*dx
L = f*v*dx
A, b = assemble_system(a,L,bc)
uh=Function(Vh)
solve(A,uh.vector(),b)

plot(uh,interactive=True)

#! /usr/bin/python

import matplotlib.pyplot as plt

from dolfin import *

import numpy as np
import logging

logging.getLogger('FFC').setLevel(logging.WARNING)
logging.getLogger('UFL').setLevel(logging.WARNING)
set_log_active(False)

np.random.seed(seed=1)

# ---

# create mesh and define function spaces
nx = 32
ny = 32
mesh = UnitSquareMesh(nx, ny)
Vm = FunctionSpace(mesh, 'Lagrange', 1)
Vu = FunctionSpace(mesh, 'Lagrange', 2)

# The true and inverted parameter
mtrue = interpolate(Expression('8. - 4.*(pow(x[0] - 0.5,2) + pow(x[1] - 0.5,2) < pow(0.2,2))', degree=5), Vm)
m_ini = interpolate(Constant(4.),Vm)

# define function for state and adjoint
u = Function(Vu)
p = Function(Vu)
m = Function(Vm)
m.assign(m_ini)

# define Trial and Test Functions
u_trial, p_trial, m_trial = TrialFunction(Vu), TrialFunction(Vu), TrialFunction(Vm)
u_test, p_test, m_test = TestFunction(Vu), TestFunction(Vu), TestFunction(Vm)

# initialize input functions
f = Constant(1.0)
u0 = Constant(0.0)

# --- 

# set up dirichlet boundary conditions
def boundary(x,on_boundary):
    return on_boundary

bc_state = DirichletBC(Vu, u0, boundary)
bc_adj = DirichletBC(Vu, Constant(0.), boundary)


# --- Need to modify for TV regularization
#     This is nonlinear ... 
# Regularization parameter
gamma = 4e-10
tv_eps = Constant(0.1)

# weak for for setting up the misfit and regularization compoment of the cost
W_equ   = inner(u_trial, u_test) * dx
R_equ   = gamma *pow( (inner(nabla_grad(m), nabla_grad(m)) + tv_eps),0.5 )*dx

W = assemble(W_equ)
R = assemble(R_equ)

# Define cost function
def cost(u, ud, a, W, R):
    diff = u.vector() - ud.vector()
    reg = 0.5 * R
    misfit = 0.5 * diff.inner(W * diff)
    return [reg + misfit, misfit, reg]

# --- 

# noise level
noise_level = 0.01
vel = Constant( (30.,0.) )

# weak form for setting up the synthetic observations
A_goal = inner( mtrue * nabla_grad(u_trial), nabla_grad(u_test)) * dx + \
          u_test*inner(vel,nabla_grad(u_trial)) * dx

L_goal = f * u_test * dx

# solve the forward/state problem to generate synthetic observations
goal_A, goal_b = assemble_system(A_goal, L_goal, bc_state)

utrue = Function(Vu)
solve(goal_A, utrue.vector(), goal_b)

ud = Function(Vu)
ud.assign(utrue)

# perturb state solution and create synthetic measurements ud
# ud = u + ||u||/SNR * random.normal
MAX = ud.vector().norm("linf")
noise = Vector()
goal_A.init_vector(noise,1)
noise.set_local( noise_level * MAX * np.random.normal(0, 1, len(ud.vector().array())) )
bc_adj.apply(noise)

ud.vector().axpy(1., noise)

## plot
#plot(utrue, title="State solution with atrue",mode="warp",interactive=True)
#plot(ud, title="Synthetic observations",mode="warp")

# --- 

# weak form for setting up the state equation
A_state = m* inner( nabla_grad(u_trial), nabla_grad(u_test)) * dx + \
          u_test*inner(vel,nabla_grad(u_trial)) * dx

L_state = f * u_test * dx

# weak form for setting up the adjoint equations
A_adj = m*inner( nabla_grad(p_trial), nabla_grad(p_test) ) * dx  + \
               p_trial * inner(vel,nabla_grad(p_test))*dx
L_adj = -inner(u - ud, p_test) * dx


# weak form for setting up matrices
g = Function(Vm)
RES   =    m_test*inner( grad(u), grad(p)) * dx + \
           gamma*pow( (inner(grad(m), grad(m))+tv_eps),-0.5 )*\
                      inner(grad(m), grad(m_test)) * dx - inner(g,m_test)*dx


# solve state equation
A, state_b = assemble_system (A_state, L_state, bc_state)
solve (A, u.vector(), state_b)

# --- 

# evaluate cost
[cost_old, misfit_old, reg_old] = cost(u, ud, m_ini, W, R)

# plot
plot(m_ini, title="m_ini",mode="warp")
plot(u, title="u(m_ini)",mode="warp")

# ---

# define parameters for the optimization
tol = 1e-4
maxiter = 4000
plot_any = 30
c_armijo = 1e-5

# initialize iter counters
iter = 1
converged = False

# initializations
m_prev = Function(Vm)

print "Nit  cost          misfit        reg         ||grad||       alpha  N backtrack"

while iter <  maxiter and not converged:

#    # solve state equation
#    A, state_b = assemble_system (A_state, L_state, bc_state)
#    solve (A, u.vector(), state_b)

    # solve the adoint problem
    adj_A, adjoint_RHS = assemble_system(A_adj, L_adj, bc_adj)
    solve(adj_A, p.vector(), adjoint_RHS)

    # evaluate the  gradient
    solve(RES == 0, g) 

    # calculate the norm of the gradient
    gradnorm = norm(g,'l2')
    grad_norm2=pow(gradnorm,2)
    
    if iter == 1:
        gradnorm0 = gradnorm

    # linesearch
    it_backtrack = 0
    m_prev.assign(m)
    alpha = 8.e5
    backtrack_converged = False
    for it_backtrack in range(20):
        
        m.vector().axpy(-alpha, g.vector() )

        # solve the state/forward problem
        state_A, state_b = assemble_system(A_state, L_state, bc_state)
        solve(state_A, u.vector(), state_b)

        # evaluate cost
        R = assemble(R_equ)
        [cost_new, misfit_new, reg_new] = cost(u, ud, m, W, R)

        # check if Armijo conditions are satisfied
        if cost_new < cost_old - alpha * c_armijo * grad_norm2:
            cost_old = cost_new
            backtrack_converged = True
            break
        else:
            alpha *= 0.5
            m.assign(m_prev)  # reset a
            
    if backtrack_converged == False:
        print "Backtracking failed. A sufficient descent direction was not found"
        converged = False
        break

    sp = ""
    print "%3d %1s %8.5e %1s %8.5e %1s %8.5e %1s %8.5e %1s %8.5e %1s %3d" % \
        (iter, sp, cost_new, sp, misfit_new, sp, reg_new, sp, \
        gradnorm, sp, alpha, sp, it_backtrack)

    if (iter % plot_any)==0 :
        plot(m, title="m",mode="warp")
    
    # check for convergence
    if gradnorm < tol*gradnorm0 and iter > 1:
        converged = True
        print "Steepest descent converged in ",iter,"  iterations"
        
    iter += 1
    
if not converged:
    print "Steepest descent did not converge in ", maxiter, " iterations"


# --- 

#plot(mtrue, title="mtrue",mode="warp",interactive=True)
plot(m, title="m",mode="warp",interactive=True)

# --- 

plot(u, title="u",mode="warp",interactive=True)
plot(p, title="p",mode="warp",interactive=True)
